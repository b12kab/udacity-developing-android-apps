# Movie Review App


##Database
This application uses [Android Realm](https://realm.io/news/realm-for-android/) as it's database.

##Optional Components
This application does not implement the optional functionality.

##API KEY
Place your API key in TmdbInfo.java:

    public static String APIKEY = "<Place your key here>";
    
##LICENSE

Copyright 2015, 2016 Keith Beatty
Unless I have authoried you, you have no permission to this code. So far, I have given no authorizations yet.
This program is distributed WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.