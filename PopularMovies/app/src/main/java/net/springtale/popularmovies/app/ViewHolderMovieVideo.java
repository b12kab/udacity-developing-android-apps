package net.springtale.popularmovies.app;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Keith on 12/22/15.
 */
public class ViewHolderMovieVideo extends RecyclerView.ViewHolder implements View.OnClickListener {
    String TAG = ViewHolderMovieVideo.class.getSimpleName();
    private MovieVideoClickListener mMovieVideoClickListener;

    // each data item is just a string in this case
    public TextView youtubeTitle;
    public ImageView playIcon;
    public LinearLayout list;
    public View layout;
    public int getAdapterPosition = 0;

    public ViewHolderMovieVideo(View v, MovieVideoClickListener movieVideoClickListener) {
        super(v);
        v.setOnClickListener(this);
        mMovieVideoClickListener = movieVideoClickListener;
        layout = v;
        list = (LinearLayout) v.findViewById(R.id.item_view_list);
        youtubeTitle = (TextView) v.findViewById(R.id.youtube_title);
        playIcon = (ImageView) v.findViewById(R.id.play_icon);
    }

    @Override
    public void onClick(View v) {
//        Log.d(TAG, "ViewHolderMovieVideo onClick - viewholder position = " + getAdapterPosition());
        // Get the position of the item selected.
        getAdapterPosition = getAdapterPosition();
        // Reduce the count by one, as the detail is the 1st position (item 0)
        getAdapterPosition--;
        mMovieVideoClickListener.onItemClick(getAdapterPosition, v);
    }
}
