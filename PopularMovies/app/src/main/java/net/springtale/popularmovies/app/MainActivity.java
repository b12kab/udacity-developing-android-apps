package net.springtale.popularmovies.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import net.springtale.popularmovies.app.TMDB.DiscoverMovieParcelable;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class MainActivity extends AppCompatActivity implements DetailActivityCall {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String DETAILFRAGMENT_TAG = "DFTAG";
    public static final String DETAIL_FILENAME_FOR_INTENT = "detailImageIntent.png";
    public static final String DETAIL_FILENAME_FOR_FRAGMENT = "detailImageFragment.png";

    private boolean mTwoPane;
    private static boolean firstLoad = true;
    private int mGridSelectPosition = 0;

    private DetailActivityFragment twoFragmentDetailActivityFragment = null;

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        Log.d(TAG, "onCreate called");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // This will get rid of the drawer shadow, as we aren't using it
        getSupportActionBar().setElevation(0f);

        mTwoPane = false;
        if (findViewById(R.id.main_detail_container) != null) {
//            Log.d(TAG, "onCreate mTwoPane is true");
            // The detail container view will be present only in the large-screen layouts.
            // If this view is present, then the activity should be in two-pane mode.
            mTwoPane = true;
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            FragmentManager fm = getSupportFragmentManager();
            if (fm != null) {
                Fragment fragment = fm.findFragmentByTag(DETAILFRAGMENT_TAG);
                if (fragment == null) {
//                    Log.d(TAG, "onCreate Fragment doesn't exist");
                    twoFragmentDetailActivityFragment = new DetailActivityFragment();
                    fm.beginTransaction().add(R.id.main_detail_container,
                            twoFragmentDetailActivityFragment,
                            DETAILFRAGMENT_TAG).commit();
                } else {
//                    Log.d(TAG, "onCreate Fragment already exists");
                    if (twoFragmentDetailActivityFragment == null) {
                        twoFragmentDetailActivityFragment = (DetailActivityFragment) fragment;
                        // This doesn't work with fragment.isHidden() check and then .show()
                        // hopefully this way will work.
                        fm.beginTransaction().replace(R.id.main_detail_container,
                                    twoFragmentDetailActivityFragment,
                                    DETAILFRAGMENT_TAG).commit();
                    }
                }
//            } else {
//                Log.e(TAG, "onCreate FragmentManager is null");
            }
//        } else {
//            Log.d(TAG, "onCreate mTwoPane is false");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_menu_about) {
            startActivity(new Intent(this, About.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    protected void onRestart() {
//        Log.d(TAG, "onRestart");
//        super.onRestart();
//    }
//
//    @Override
//    protected void onStart() {
//        Log.d(TAG, "onStart");
//        super.onStart();
//    }
//
//    @Override
//    protected void onPause() {
//        Log.d(TAG, "onPause");
//        super.onPause();
//    }
//
//    @Override
//    protected void onStop() {
//        Log.d(TAG, "onStop");
//        super.onStop();
//    }
//
//    @Override
//    protected void onDestroy() {
//        Log.d(TAG, "onDestroy");
//        super.onDestroy();
//    }

    /***
     * This will tell the detail fragment, if this is the main activity or not
     * @return
     */
    @Override
    public boolean doesMainActivityExist() {
        return true;
    }

    /***
     * This will be called when the movie item is touched in the main activity fragment
     * @param movieImage
     * @param discoverMovieParcelable
     * @param position
     * @param id
     */
    public void onItemSelected(Bitmap movieImage, DiscoverMovieParcelable discoverMovieParcelable,
                               int position, long id) {
//        Log.d(TAG, "onItemSelected pos: " + position);
        mGridSelectPosition = position;


        if (mTwoPane) {
            setDetailFragData(discoverMovieParcelable, movieImage);

        } else {
            saveFileForDetailedIntent(movieImage, DETAIL_FILENAME_FOR_INTENT);
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailActivity.INTENT_EXTRA_LOCAL_IMAGE_FILE_NAME, DETAIL_FILENAME_FOR_INTENT);
            intent.putExtra(DetailActivity.INTENT_EXTRA_MOVIE, discoverMovieParcelable);
            intent.putExtra(DetailActivity.INTENT_EXTRA_POSITION, position);
            startActivity(intent);
        }
    }

    /***
     * We can't pass the image on the intent, so have to save it on the filesystem so that the detail
     * activity can read it in.
     * @param movieImage Bitmap to save
     */
    public void saveFileForDetailedIntent(Bitmap movieImage, String detailFileName) {
//        Log.d(TAG, "saveFileForDetailedIntent");
        // https://stackoverflow.com/questions/3528735/failed-binder-transaction
        // https://stackoverflow.com/questions/8407336/how-to-pass-drawable-between-activities
        // Have to compress image
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        movieImage.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] compressedImage = byteArrayOutputStream.toByteArray();

        try {
            FileOutputStream fileOutStream = openFileOutput(detailFileName, MODE_PRIVATE);
            fileOutStream.write(compressedImage);  //b is byte array
            //(used if you have your picture downloaded
            // from the *Web* or got it from the *devices camera*)
            //otherwise this technique is useless
            fileOutStream.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public int getListPosition() {
        return mGridSelectPosition;
    }

    /***
     * This method tells the caller if the detail fragment is attached to the activity
     * @return
     */
    public boolean isDetailFragmentAttached() {
        boolean hasFragment = false;

        FragmentManager fm = getSupportFragmentManager();
        if (fm != null) {
            Fragment fragment = fm.findFragmentByTag(DETAILFRAGMENT_TAG);
            if (fragment != null) {
                hasFragment = true;
            }
        }

        return hasFragment;
    }

    /***
     * This sets the movie detail fragment data, if it exists
     * @param movieParcelable
     * @param bitmap
     */
    public void setDetailFragData(DiscoverMovieParcelable movieParcelable, Bitmap bitmap) {
//        Log.d(TAG, "setDetailFragData");
        FragmentManager fm = getSupportFragmentManager();
        if (fm != null) {
            Fragment fragment = fm.findFragmentByTag(DETAILFRAGMENT_TAG);
            if (fragment != null) {
                if (twoFragmentDetailActivityFragment == null) {
                    twoFragmentDetailActivityFragment = (DetailActivityFragment) fragment;
                }
                twoFragmentDetailActivityFragment.setFavoriteStatus(DetailActivityFragment.favoriteChangeType.UNSET);
                twoFragmentDetailActivityFragment.setDetailInformation(movieParcelable, true, bitmap, false, null, true, true);
            }
        }
    }

    /***
     * This will hide the detail fragment, if there isn't any movies to show
     */
    public void hideDetailFragment() {
//        Log.d(TAG, "hideDetailFragment");
        FragmentManager fm = getSupportFragmentManager();
        if (fm != null && mTwoPane) {
            Fragment fragment = fm.findFragmentByTag(DETAILFRAGMENT_TAG);
            if (fragment != null) {
//                Log.d(TAG, "hideDetailFragment - found detail fragment, visible, about to hide it");
                fm.beginTransaction().hide(fragment).commit();
            }
        }
    }

    /***
     * This will show the detail fragment, if movie detail can be shown
     */
    public void showDetailFragment() {
//        Log.d(TAG, "showDetailFragment");
        FragmentManager fm = getSupportFragmentManager();
        if (fm != null && mTwoPane) {
            Fragment fragment = fm.findFragmentByTag(DETAILFRAGMENT_TAG);
            if (fragment != null) {
//                Log.d(TAG, "showDetailFragment - found detail fragment, hidden, about to show it");
                fm.beginTransaction().show(fragment).commit();
            }
        }
    }

    /***
     * This is from the DetailActivityCall, if there exists the two pane view for this activity
     * @return
     */
    @Override
    public boolean isInTwoPaneMode() {
        return mTwoPane;
    }
}
