package net.springtale.popularmovies.app.TMDB;

import com.uwetrottmann.tmdb.Tmdb;

/**
 * Created by Keith on 10/6/15.
 */
public class TmdbInfo {
    public static String APIKEY = "<Place your key here>";
    public static Tmdb tmdb;
    public static int MIN_VOTE_COUNT = 20;
}
