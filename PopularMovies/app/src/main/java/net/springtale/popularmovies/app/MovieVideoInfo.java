package net.springtale.popularmovies.app;

import android.net.Uri;

/**
 * Created by Keith on 12/1/15.
 */
public class MovieVideoInfo {
    Uri videoUri;
    String youtubeTitle;

    public Uri getVideoUri() {
        return videoUri;
    }

    public void setVideoUri(Uri videoUri) {
        this.videoUri = videoUri;
    }

    public String getYoutubeTitle() {
        return youtubeTitle;
    }

    public void setYoutubeTitle(String youtubeTitle) {
        this.youtubeTitle = youtubeTitle;
    }
}
