package net.springtale.popularmovies.app.TMDB;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.uwetrottmann.tmdb.entities.Movie;

import net.springtale.popularmovies.app.MovieFavorite;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmIOException;
import io.realm.exceptions.RealmMigrationNeededException;

/**
 * Created by Keith on 1/1/16.
 */
public class IntentServiceObtainFavoriteMovies extends IntentService {
    private static final String TAG = IntentServiceObtainFavoriteMovies.class.getSimpleName();
    boolean mErrorOccurred;
    String mErrorMessage = "";
    private ArrayList<DiscoverMovieParcelable> tmdbPopularMovies = null;
    private ArrayList<DiscoverMovieParcelable> tmdbHighestRated = null;
    private ArrayList<DiscoverMovieParcelable> tmdbFavoriteMovies = null;

    /***
     * Global variables
     */
    public static final String INTENT_INPUT_POPULAR_MOVIES_ARRAYLIST = "popular_movies_array_list";
    public static final String INTENT_INPUT_HIGH_RATED_MOVIES_ARRAYLIST = "popular_highest_rated_movies_array_list";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public IntentServiceObtainFavoriteMovies() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
//        Log.d(TAG, "onHandleIntent");

        Bundle bundle = intent.getExtras();
        if (null != bundle) {
            tmdbPopularMovies = bundle.getParcelableArrayList(INTENT_INPUT_POPULAR_MOVIES_ARRAYLIST);
            tmdbHighestRated = bundle.getParcelableArrayList(INTENT_INPUT_HIGH_RATED_MOVIES_ARRAYLIST);
        } else {
            mErrorMessage = "Missing one or both ArrayLists";
            mErrorOccurred = true;
        }

        if (!mErrorOccurred) {
            getFavorites();
        }

        sendReply();
    }

    /***
     * Make the favorite movie list
     */
    private void getFavorites() {
//        Log.d(TAG, "getFavorites");
        ArrayList<Integer> movieIds = getMovieFavoriteIds();
        if (mErrorOccurred) {
            return;
        }

        tmdbFavoriteMovies = new ArrayList<DiscoverMovieParcelable>();
        DiscoverMovieParcelable searchResult = null;

        for (int movieId : movieIds) {
            searchResult = searchPopularMovies(movieId);
            if (searchResult == null) {
                searchResult = searchHighestRatedMovies(movieId);
                if (searchResult == null) {
                    searchResult = getMovieId(movieId);
                    //noinspection StatementWithEmptyBody
                    if (searchResult == null) {
//                        Log.d(TAG, "Failed to find movie id = " + movieId);
                    } else {
                        tmdbFavoriteMovies.add(searchResult);
                    }
                } else {
                    tmdbFavoriteMovies.add(searchResult);
                }
            } else {
                tmdbFavoriteMovies.add(searchResult);
            }
        }
    }

    /***
     * Get the user's favorite movie id's
     *
     * @return
     */
    private ArrayList<Integer> getMovieFavoriteIds() {
        ArrayList<Integer> movieIds = new ArrayList<Integer>();

        try {
            Realm realm = Realm.getInstance(getBaseContext());
            RealmResults<MovieFavorite> results = realm.allObjects(MovieFavorite.class);
            for (int i = 0; i < results.size(); i++) {
                movieIds.add(results.get(i).getMovieId());
            }
            // close the realm instance
            realm.close();
        } catch (IllegalArgumentException iae) {
            mErrorOccurred = true;
            mErrorMessage = "Error - IllegalArgumentException " + iae.toString();
            Log.e(TAG, mErrorMessage);
        } catch (RealmMigrationNeededException rmne) {
            mErrorOccurred = true;
            mErrorMessage = "Error - RealmMigrationNeededException - RealmObject classes no longer match the underlying Realm and it must be migrated.";
            Log.e(TAG, mErrorMessage);
        } catch (RealmIOException rioe) {
            mErrorOccurred = true;
            mErrorMessage = "Error - RealmIOException - " + rioe.toString();
            Log.e(TAG, mErrorMessage);
        } catch (Exception e) {
            mErrorOccurred = true;
            mErrorMessage = "Error " + e.toString();
            Log.e(TAG, mErrorMessage);
        }

        return movieIds;
    }

    /***
     * Get the movie from our discovery popular movie list
     *
     * @param movieId
     * @return
     */
    @Nullable
    private DiscoverMovieParcelable searchPopularMovies(int movieId) {
        DiscoverMovieParcelable returnMovie = null;

        // Only try on array lists that have data
        if (tmdbPopularMovies != null) {
            for (int i = 0; i < tmdbPopularMovies.size(); i++) {
                if (tmdbPopularMovies.get(i).getId() == movieId) {
                    returnMovie = tmdbPopularMovies.get(i);
                    break;
                }
            }
        }

        return returnMovie;
    }

    /***
     * Get the movie from our discovery highest rated movie list
     *
     * @param movieId
     * @return
     */
    @Nullable
    private DiscoverMovieParcelable searchHighestRatedMovies(int movieId) {
        DiscoverMovieParcelable returnMovie = null;

        // Only try on array lists that have data
        if (tmdbHighestRated != null) {
            for (int i = 0; i < tmdbHighestRated.size(); i++) {
                if (tmdbHighestRated.get(i).getId() == movieId) {
                    returnMovie = tmdbHighestRated.get(i);
                    break;
                }
            }
        }

        return returnMovie;
    }

    /***
     * Get the movie information from the TMDB provider
     *
     * @param movieId
     * @return
     */
    @Nullable
    private DiscoverMovieParcelable getMovieId(int movieId) {
        DiscoverMovieParcelable returnMovie = null;

        Movie movieReturn = TmdbInfo.tmdb.moviesService().summary(movieId, "", null);
        if (movieReturn != null && movieReturn.id != 0) {
            returnMovie = DiscoverMovieParcelable.createNewDiscoverMovie(movieReturn);
        }

        return returnMovie;
    }

    private boolean replySent = false;
    public static final String INTENT_NAME = "MovieFavorite.result";
    public static final String INTENT_EXTRA_STATUS = "status";
    public static final String INTENT_EXTRA_ERROR_MSG = "favorite_movies_error_msg";
    public static final String INTENT_EXTRA_FAVORITES = "favorite_movies_parsable_arraylist";
    public static final String FAVORITE_RETRIEVE_WORKED = "GOOD";
    public static final String FAVORITE_RETRIEVE_ERROR = "ERROR";
    public static final String FAVORITE_RETRIEVE_WORKED_NO_RESULT = "NO_DATA";

    /***
     * Create the result to send back to the Service caller.
     */
    private void sendReply() {
//        Log.d(TAG, "sendReply ");

        Intent intent = new Intent(INTENT_NAME);
        String resultType;
        String errorMessage = "";

        if (mErrorOccurred) {
            resultType = FAVORITE_RETRIEVE_ERROR;
            errorMessage = mErrorMessage;
        } else if (tmdbFavoriteMovies == null || tmdbFavoriteMovies.size() == 0) {
            resultType = FAVORITE_RETRIEVE_WORKED_NO_RESULT;
            if (tmdbFavoriteMovies == null) {
                errorMessage = "Movie favorites is null";
            } else if (tmdbFavoriteMovies.size() == 0) {
                errorMessage = "Movie favorites size = 0";
            }
        } else {
            resultType = FAVORITE_RETRIEVE_WORKED;
        }

        intent.putExtra(INTENT_EXTRA_STATUS, resultType);
        if (!errorMessage.isEmpty()) {
            intent.putExtra(INTENT_EXTRA_ERROR_MSG, errorMessage);
        }
        if (tmdbFavoriteMovies != null || tmdbFavoriteMovies.size() > 0) {
            intent.putParcelableArrayListExtra(INTENT_EXTRA_FAVORITES, tmdbFavoriteMovies);
        }
//        Log.d(TAG, "sendReply - tmdbFavoriteMovies size " + tmdbFavoriteMovies.size());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        // reset flag
        replySent = true;
    }
}
