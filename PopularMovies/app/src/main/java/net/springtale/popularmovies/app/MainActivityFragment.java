package net.springtale.popularmovies.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.uwetrottmann.tmdb.Tmdb;
import com.uwetrottmann.tmdb.entities.Configuration;
import com.uwetrottmann.tmdb.enumerations.SortBy;

import net.springtale.popularmovies.app.TMDB.DiscoverMovieParcelable;
import net.springtale.popularmovies.app.TMDB.IntentServiceDiscoverMovie;
import net.springtale.popularmovies.app.TMDB.IntentServiceObtainFavoriteMovies;
import net.springtale.popularmovies.app.TMDB.TmdbInfo;

import java.util.ArrayList;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment //implements DiscoverFetchReturnTmdb
{
    private static final String TAG = MainActivityFragment.class.getSimpleName();
    private boolean mMoviesAlreadyLoaded = false;
    private static boolean firstTimeDataPullToRefresh = true;

    private String tmdbBaseUrl = null;
    private String tmdbSecureBaseUrl = null;
    private int mSavedGridLocation = -9;
    private int mDropDownMenuPos = 0;

    private ArrayList<DiscoverMovieParcelable> tmdbPopularMovies = null;
    private ArrayList<DiscoverMovieParcelable> tmdbHighestRated = null;
    private ArrayList<DiscoverMovieParcelable> tmdbFavorites = null;
    private ArrayList<String> tmdbBackdropSizes = null;
    private ArrayList<String> tmdbLogoSizes = null;
    private ArrayList<String> tmdbPosterSizes = null;
    private ArrayList<String> tmdbProfileSizes = null;

    public enum showMovieType { POPULAR, RATING, FAVORITES, NOT_SET }
    private final showMovieType DEFAULT_MENU_SELECTION = showMovieType.POPULAR;

    private MainActivity mainActivity = null;
    private showMovieType sortType;
    private showMovieType initSortType = null;
    private boolean setInitSortType;

//  example
//    http://code.tutsplus.com/tutorials/android-sdk-working-with-picasso--cms-22149
    private GridView mGridView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private DataAdapter dataAdapter;

    private static int popularPosition;
    private static int ratingPosition;
    private static int favoritePosition;
    Spinner mMovieTypeSpinner = null;

    // These are key value items that are saved off when the activity restarts
    private static final String SAVED_STATE_POPULAR_KEY = "popular_key";
    private static final String SAVED_STATE_RATING_KEY = "rating_key";
    private static final String SAVED_STATE_FAVORITE_KEY = "favorite_key";
    private static final String SAVED_STATE_BASE_URL_KEY = "tmdb_base_url";
    private static final String SAVED_STATE_SECURE_BASE_URL_KEY = "tmdb_secure_base_url";
    private static final String SAVED_STATE_BACKDROP_SIZE_KEY = "tmdb_backdrop_sizes";
    private static final String SAVED_STATE_LOGO_SIZE_KEY = "tmdb_logo_sizes";
    private static final String SAVED_STATE_POSTER_SIZE_KEY = "tmdb_poster_sizes";
    private static final String SAVED_STATE_PROFILE_SIZE_KEY = "tmdb_profile_sizes";
    private static final String SAVED_STATE_GRID_LOCATION = "main_activity_grid_location";
    private static final String SAVED_STATE_POSITION_MOVIE_DROPDOWN = "MAIN_ACTIVITY_POSITION_DROP_DOWN";
    private static final int SAVED_STATE_POSITION_INVALID_POS = -1;

    private static final int DISCOVER_MOVIE_START_PAGE = 1;
    private static final int DISCOVER_MOVIE_END_PAGE = 3;

    public MainActivityFragment() {
    }

    // Activity create - fragment lifecycle created 1
    @Override
    public void onAttach(Activity activity) {
//        Log.d(TAG, "onAttach called from - " + activity.getLocalClassName());
        super.onAttach(activity);
        mainActivity = (MainActivity) activity;
    }

    /***
     * This will get the configuration information
     */
    private void getConfigInfo() {
//        Log.d(TAG, "getConfigInfo");

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Configuration config = TmdbInfo.tmdb.configurationService().configuration();

                    // Copy the data over to the base variables
                    tmdbBaseUrl = config.images.base_url;
                    tmdbSecureBaseUrl = config.images.secure_base_url;
                    tmdbBackdropSizes = new ArrayList<String>();
                    tmdbBackdropSizes.addAll(config.images.backdrop_sizes);
                    tmdbLogoSizes = new ArrayList<String>();
                    tmdbLogoSizes.addAll(config.images.logo_sizes);
                    tmdbPosterSizes = new ArrayList<String>();
                    tmdbPosterSizes.addAll(config.images.poster_sizes);
                    tmdbProfileSizes = new ArrayList<String>();
                    tmdbProfileSizes.addAll(config.images.profile_sizes);
                } catch (RetrofitError re) {
                    Response r = re.getResponse();
                    int httpStatus = r.getStatus();
                    Log.e(TAG, String.valueOf(httpStatus) + " Http status on getting TMDB configuration");
                    Log.e(TAG, r.toString());
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (Exception e) {
            Log.e(TAG, "Error " + e.toString(), e);
        }
    }

    // Activity create - fragment lifecycle created 2
    @Override
    public void onCreate( Bundle savedInstanceState) {
//        Log.d(TAG, "onCreate called");
        super.onCreate(savedInstanceState);
        // https://gist.github.com/curioustechizen/6ed0981b013f63236f0b maybe???

        setHasOptionsMenu(true);

        boolean worked = setMovieTypePositions();
        if (!worked) {
            Log.e(TAG, "onCreate Failed to load movie type positions");
        }

        mDropDownMenuPos = SAVED_STATE_POSITION_INVALID_POS;

        if (savedInstanceState != null) {
//            Log.d(TAG, "onCreate savedInstanceState != null");
            if (savedInstanceState.containsKey(SAVED_STATE_POPULAR_KEY)) {
                tmdbPopularMovies = savedInstanceState.getParcelableArrayList(SAVED_STATE_POPULAR_KEY);
            }
            if (savedInstanceState.containsKey(SAVED_STATE_RATING_KEY)) {
                tmdbHighestRated = savedInstanceState.getParcelableArrayList(SAVED_STATE_RATING_KEY);
            }
            if (savedInstanceState.containsKey(SAVED_STATE_FAVORITE_KEY)) {
                tmdbFavorites = savedInstanceState.getParcelableArrayList(SAVED_STATE_FAVORITE_KEY);
            }

            if (savedInstanceState.containsKey(SAVED_STATE_BASE_URL_KEY)) {
                tmdbBaseUrl = savedInstanceState.getString(SAVED_STATE_BASE_URL_KEY);
            }

            if (savedInstanceState.containsKey(SAVED_STATE_SECURE_BASE_URL_KEY)) {
                tmdbSecureBaseUrl = savedInstanceState.getString(SAVED_STATE_SECURE_BASE_URL_KEY);
            }

            if (savedInstanceState.containsKey(SAVED_STATE_BACKDROP_SIZE_KEY)) {
                tmdbBackdropSizes = savedInstanceState.getStringArrayList(SAVED_STATE_BACKDROP_SIZE_KEY);
            }

            if (savedInstanceState.containsKey(SAVED_STATE_LOGO_SIZE_KEY)) {
                tmdbLogoSizes = savedInstanceState.getStringArrayList(SAVED_STATE_LOGO_SIZE_KEY);
            }

            if (savedInstanceState.containsKey(SAVED_STATE_POSTER_SIZE_KEY)) {
                tmdbPosterSizes = savedInstanceState.getStringArrayList(SAVED_STATE_POSTER_SIZE_KEY);
            }

            if (savedInstanceState.containsKey(SAVED_STATE_PROFILE_SIZE_KEY)) {
                tmdbProfileSizes = savedInstanceState.getStringArrayList(SAVED_STATE_PROFILE_SIZE_KEY);
            }

            if (savedInstanceState.containsKey(SAVED_STATE_GRID_LOCATION)) {
                mSavedGridLocation = savedInstanceState.getInt(SAVED_STATE_GRID_LOCATION);
//                Log.d(TAG, "onCreate getInt getFirstVisiblePos: " + mSavedGridLocation);
            }

            if (savedInstanceState.containsKey(SAVED_STATE_POSITION_MOVIE_DROPDOWN)) {
                mDropDownMenuPos = savedInstanceState.getInt(SAVED_STATE_POSITION_MOVIE_DROPDOWN);
//                Log.d(TAG, "onCreate Saved SAVED_STATE_POSITION_MOVIE_DROPDOWN = " + mDropDownMenuPos);
            }

            // Note: can't set the data here as the data adapter doesn't yet exist
            mMoviesAlreadyLoaded = true;

            // Don't set the sort type when we can get it later (in onStart)
            setInitSortType = false;
        } else {
//            Log.d(TAG, "onCreate savedInstanceState IS null");
            mMoviesAlreadyLoaded = false;
            mSavedGridLocation = -9;

            // Set the TMDB data
            TmdbInfo.tmdb = new Tmdb();
            TmdbInfo.tmdb.setApiKey(TmdbInfo.APIKEY);
//            TmdbInfo.tmdb.setIsDebug(true);

            // Get the TMDB configuration data
            getConfigInfo();

            // Don't set the sort type when we can get it later (in onStart)
            setInitSortType = true;

            // Start getting the movie data
            getAllMovieSelectionTypes();
        }
        initSortType = null;
    }

    // Activity create - fragment lifecycle created 3
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        Log.d(TAG, "onCreateView");
        setHasOptionsMenu(true);

        // Deal with view stuff
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.main_swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.Accent);
        mSwipeRefreshLayout.setEnabled(true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requeryTMDBDiscoveryMovieData();
            }
        });

        if (!mMoviesAlreadyLoaded) {
            startPullToRefresh();
        }

        // create Adapter
        dataAdapter = new DataAdapter(getActivity());
        dataAdapter.setPictureBaseURL(tmdbSecureBaseUrl);
        dataAdapter.setSizeURLPortion(tmdbPosterSizes.get(1));
        dataAdapter.setInflater(inflater);
        Drawable missingPoster = inflater.getContext().getResources().getDrawable(R.drawable.movie_poster_w154_missing);
        dataAdapter.setMissingPoster(missingPoster);

        // Create the GridView
        mGridView =(GridView) view.findViewById(R.id.grid);
        mGridView.setAdapter(dataAdapter);

        // set the listener action
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
//                Log.d(TAG, "onCreateView mGridView - onItemClick");
                final DiscoverMovieParcelable movie = dataAdapter.getMovieDataAtPos(position);

                String networkMoviePicLocation = dataAdapter.getMovieImageUriString(position);
//                Log.d(TAG, "onCreateView - setOnItemClickListener URI: " + networkMoviePicLocation);
                Ion.with(getActivity()).load(networkMoviePicLocation).withBitmap().asBitmap()
                        .setCallback(new FutureCallback<Bitmap>() {
                            @Override
                            public void onCompleted(Exception e, Bitmap result) {
//                                Log.d(TAG, "onCreateView - setOnItemClickListener - Ion.onCompleted:: width: " +
//                                        result.getWidth() + " height: " + result.getHeight() );
                                mainActivity.onItemSelected(result, movie, position, id);
                            }
                        });
            }
        });

        return view;
    }

    // Activity create - fragment lifecycle created 4
    /***
     * Without the getLoaderManager, the loader will not work :(
     *
     * @param savedInstanceState saved bundle
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        Log.d(TAG, "onActivityCreated started");
        super.onActivityCreated(savedInstanceState);
    }

    // Fragment ending to save off data
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
//        Log.d(TAG, "onSaveInstanceState");
        // According to https://stackoverflow.com/questions/15313598/once-for-all-how-to-correctly-save-instance-state-of-fragments-in-back-stack
        // super done 1st
        super.onSaveInstanceState(savedInstanceState);

        if (tmdbPopularMovies!= null) {
            savedInstanceState.putParcelableArrayList(SAVED_STATE_POPULAR_KEY, tmdbPopularMovies);
        }

        if (tmdbHighestRated != null) {
            savedInstanceState.putParcelableArrayList(SAVED_STATE_RATING_KEY, tmdbHighestRated);
        }

        if (tmdbFavorites != null) {
            savedInstanceState.putParcelableArrayList(SAVED_STATE_FAVORITE_KEY, tmdbFavorites);
        }

        if (tmdbBaseUrl != null) {
            savedInstanceState.putString(SAVED_STATE_BASE_URL_KEY, tmdbBaseUrl);
        }

        if (tmdbSecureBaseUrl != null) {
            savedInstanceState.putString(SAVED_STATE_SECURE_BASE_URL_KEY, tmdbSecureBaseUrl);
        }

        if (tmdbBackdropSizes != null) {
            savedInstanceState.putStringArrayList(SAVED_STATE_BACKDROP_SIZE_KEY, tmdbBackdropSizes);
        }

        if (tmdbLogoSizes != null) {
            savedInstanceState.putStringArrayList(SAVED_STATE_LOGO_SIZE_KEY, tmdbLogoSizes);
        }

        if (tmdbPosterSizes != null) {
            savedInstanceState.putStringArrayList(SAVED_STATE_POSTER_SIZE_KEY, tmdbPosterSizes);
        }

        if (tmdbProfileSizes != null) {
            savedInstanceState.putStringArrayList(SAVED_STATE_PROFILE_SIZE_KEY, tmdbProfileSizes);
        }

        if (mGridView != null) {
            int getFirstVisiblePos = mGridView.getFirstVisiblePosition();
//            Log.d(TAG, "onSaveInstanceState getFirstVisiblePos: " + getFirstVisiblePos);
            savedInstanceState.putInt(SAVED_STATE_GRID_LOCATION, getFirstVisiblePos);
        }

        int movieDropDownPosition = getMovieDropDownPosition();
        if (movieDropDownPosition != SAVED_STATE_POSITION_INVALID_POS && movieDropDownPosition >= 0) {
            savedInstanceState.putInt(SAVED_STATE_POSITION_MOVIE_DROPDOWN, movieDropDownPosition);
        }
    }

    // Fragment Activity start
    @Override
    public void onStart() {
//        Log.d(TAG, "onStart");
        super.onStart();

        sortType = getMovieType();
//        Log.d(TAG, "onStart sortType: " + sortType);

        // This is here as we can't get the movie drop down data (initially) until onStart()
        if (setInitSortType) {
            setInitSortType = false;

            if (sortType == showMovieType.NOT_SET) {
                // Check to see if the saved location is unset.
                sortType = getMovieTypeByPos(mDropDownMenuPos);
            }
            initSortType = sortType;
        } else {
            if (sortType == showMovieType.NOT_SET) {
                // Check to see if the saved location is unset.
                sortType = getMovieTypeByPos(mDropDownMenuPos);
            }
        }

        if (mMoviesAlreadyLoaded) {
            setMovieList();
        }

        // if we have changed configuration, and not on 1st time, restore the location
        if (mSavedGridLocation > 0) {
//            Log.d(TAG, "onStart - mSavedGridLocation: " + mSavedGridLocation);
            mGridView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mGridView.setSelection(mSavedGridLocation);
                }
            }, 1);
        }
    }

    /**
     * This is called after onStart()
     * https://github.com/OleksandrKucherenko/binding-tc
     * https://stackoverflow.com/questions/26755878/how-can-i-fix-the-spinner-style-for-android-4-x-placed-on-top-of-the-toolbar
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        Log.d(TAG, "onCreateOptionsMenu");

        inflater.inflate(R.menu.menu_layout, menu);

        MenuItem item = menu.findItem(R.id.spinner);

        item.setActionView(R.layout.spinner_layout);
        View view = item.getActionView();

        mMovieTypeSpinner = (Spinner) view.findViewById(R.id.spinner_nav);

        // this only works after onStart finishes - sadly enough in a Fragment by my testing
        if (mDropDownMenuPos > SAVED_STATE_POSITION_INVALID_POS) {
            setMovieDropDownPositionListenerNotCalled(mDropDownMenuPos);
        }

        mMovieTypeSpinner.setOnItemSelectedListener(mSpinnerChangedReceiver);
        super.onCreateOptionsMenu(menu, inflater);
    }

    // Activity resumed - fragment lifecycle resumed
    @Override
    public void onResume() {
//        Log.d(TAG, "onResume called");
        registerLocalBroadcastLocation();

        // See if we need to display the waiting for data icon
        if (firstTimeDataPullToRefresh) {
            firstTimeDataPullToRefresh = false;
            startPullToRefresh();
        }

        super.onResume();
    }

    //  http://www.scriptscoop.net/t/d981325ece17/android-savedinstancestate-always-null.html
    @Override
    public void onPause() {
//        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
//        Log.d(TAG, "onStop");
        super.onStop();
        unregisterLocalBroadcastLocation();
    }
//
//    @Override
//    public void onDestroyView() {
//        Log.d(TAG, "onDestroyView");
//        super.onDestroyView();
//    }
//
//    @Override
//    public void onDestroy() {
//        Log.d(TAG, "onDestroy");
//        super.onDestroy();
//    }

    @Override
    public void onDetach() {
//        Log.d(TAG, "onDetach");
        super.onDetach();
//        stopMenuBarMovieListener();
    }

    /******************************************************************************************/
    /* End of UI methods*/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/

    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /* Start of helper method section */
    /******************************************************************************************/

    /***
     * This is the listener for the action bar movie drop down list
     */
    private final AdapterView.OnItemSelectedListener mSpinnerChangedReceiver = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
//            Log.d(TAG, "mSpinnerChangedReceiver - onItemSelected() called");
            mDropDownMenuPos = position;
            // On selecting a different movie spinner item
            String item = adapter.getItemAtPosition(position).toString();
            showMovieType movieType = getMovieTypeByPos(position);
//            Log.d(TAG, "mSpinnerChangedReceiver onItemSelected - pos: " + position + " item: " +
//                    item + " movie: " + movieType);
            initSortType = movieType;
            determineMenuUpdateAction(movieType);
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
        }
    };

    /***
     * This will return the action bar movie drop down list
     * @return POPULAR = "Most Popular" or RATING = "Highest Rating"
     */
    public showMovieType getMovieType() {
//        Log.d(TAG, "getMovieType");
        int listPos = getMovieDropDownPosition();

        if (listPos != SAVED_STATE_POSITION_INVALID_POS) {
            return getMovieTypeByPos(listPos);
        } else {
            return showMovieType.NOT_SET;
        }
    }

    /***
     * This takes in the position # and returns the showMovieType
     * @param menuPosition
     * @return showMovieType
     */
    private showMovieType getMovieTypeByPos(int menuPosition) {
//        Log.d(TAG, "getMovieTypeByPos");
        if (popularPosition == menuPosition) {
            return showMovieType.POPULAR;
        } else if (ratingPosition == menuPosition) {
            return showMovieType.RATING;
        } else if (favoritePosition == menuPosition) {
            return showMovieType.FAVORITES;
        } else {
            // got to return something, so return the default menu selection
            return DEFAULT_MENU_SELECTION;
        }
    }

    /***
      * Get the numeric position of the action bar movie drop down list
      * @return
      */
    private int getMovieDropDownPosition() {
//        Log.d(TAG, "getMovieDropDownPosition");
        int spinnerPosition = SAVED_STATE_POSITION_INVALID_POS;

        if (mMovieTypeSpinner != null) {
            try {
                spinnerPosition = mMovieTypeSpinner.getSelectedItemPosition();
            } catch (Exception e) {
                Log.d(TAG, "Error the spinner isn't set " + e.toString(), e);
                spinnerPosition = SAVED_STATE_POSITION_INVALID_POS;
            }
        }

        return spinnerPosition;
    }

    /***
     * Set the position of the action bar movie drop down list w/o setting the listener
     * @param position
     */
    private void setMovieDropDownPositionListenerNotCalled(int position) {
//        Log.d(TAG, "setMovieDropDownPositionListenerNotCalled - position: " + position);
        mMovieTypeSpinner.setSelection(position, false);
    }

    /***
     * Set the position of the action bar movie drop down list
     * @param position
     */
    private void setMovieDropDownPosition(int position) {
//        Log.d(TAG, "setMovieDropDownPosition - position: " + position);
        mMovieTypeSpinner.setSelection(position);
    }

    /***
     * This will set the action bar movie drop down list that displayed
     */
    private void setMovieList() {
//        Log.d(TAG, "setMovieList sortType: " + sortType);

        if (showMovieType.POPULAR == sortType) {
            if (tmdbPopularMovies != null) {
                resetDataAdapter(tmdbPopularMovies, sortType);
            }
        } else if (showMovieType.RATING == sortType) {
            if (tmdbHighestRated != null) {
                resetDataAdapter(tmdbHighestRated, sortType);
            }
        } else if (showMovieType.FAVORITES == sortType) {
            if (tmdbFavorites != null) {
                resetDataAdapter(tmdbFavorites, sortType);
            }
        }
    }

    /***
     * Determine either the selected menu item or use the default menu value
     * @return showMovieType
     */
    private showMovieType getMovieTypeOrDefault() {
        showMovieType currentMovieType = getMovieType();
        if (currentMovieType == showMovieType.NOT_SET){
            currentMovieType = DEFAULT_MENU_SELECTION;
        }
        return currentMovieType;
    }

    /***
     * This function gets the position of the items in the toolbar to use later.
     * @return true - got all of the positions needed, false - bad things happened.
     */
    private boolean setMovieTypePositions() {
//        Log.d(TAG, "setMovieTypePositions");
        boolean workedOK = false;
        boolean popularFound = false;
        boolean ratingFound = false;
        boolean favoriteFound = false;

        String[] actionBarStrings = getActivity().getResources().getStringArray(R.array.action_bar_item_list);
        for (int pos = 0; pos < actionBarStrings.length; pos++) {
            if (actionBarStrings[pos].equals(getString(R.string.toolbar_sort_popular))) {
                popularFound = true;
                popularPosition = pos;
            } else if (actionBarStrings[pos].equals(getString(R.string.toolbar_sort_rating))) {
                ratingFound = true;
                ratingPosition = pos;
            } else if (actionBarStrings[pos].equals(getString(R.string.toolbar_sort_favorite))) {
                favoriteFound = true;
                favoritePosition = pos;
//            } else {
//                Log.d(TAG, "setMovieTypePositions Found " + actionBarStrings[pos] +
//                        " while looping thru dropdown array. This shouldn't happen");
            }
        }

        if (popularFound && ratingFound && favoriteFound) {
            workedOK = true;
        }

        return workedOK;
    }

    /***
     * This converts the showMovieType to the actual string value of the drop down menu array
     * @param movieType showMovieType
     * @return string value of the drop down menu array
     */
    public String convertShowMovieTypeToString(showMovieType movieType) {
//        Log.d(TAG, "convertShowMovieTypeToString");
        String[] actionBarStrings = getActivity().getResources().getStringArray(R.array.action_bar_item_list);

        String movieTypeString = actionBarStrings[popularPosition];

        if (showMovieType.RATING == movieType) {
            movieTypeString = actionBarStrings[ratingPosition];
        } else if (showMovieType.FAVORITES == movieType) {
            movieTypeString = actionBarStrings[favoritePosition];
        }

        return movieTypeString;
    }

    /***
     * This will get the data from the TMDB values
     */
    private void getAllMovieSelectionTypes() {
//        Log.d(TAG, "getAllMovieSelectionTypes");
        discoverPopularMovies();
        discoverHighestRatedMovies();
        obtainFavorites();
    }

    /***
     * This will get the popular movies
     */
    private void discoverPopularMovies() {
//        Log.d(TAG, "discoverPopularMovies");
        discoverPopularMovies(DISCOVER_MOVIE_START_PAGE, DISCOVER_MOVIE_END_PAGE);
    }

    /***
     * This will get the popular movies from the TMDB service
     * @param startPage
     * @param endPage
     */
    private void discoverPopularMovies(int startPage, int endPage) {
//        Log.d(TAG, "discoverPopularMovies - start:" + startPage + " end: " + endPage);
        // Start the service to get popular movies in descending order
        discoverMovie(startPage, endPage, SortBy.POPULARITY_DESC);
    }

    /***
     * This will get the highest rated movies
     */
    private void discoverHighestRatedMovies() {
//        Log.d(TAG, "discoverHighestRatedMovies");
        discoverHighestRatedMovies(DISCOVER_MOVIE_START_PAGE, DISCOVER_MOVIE_END_PAGE);
    }

    /***
     * This will get the highest rated movies from the TMDB service
     * @param startPage
     * @param endPage
     */
    private void discoverHighestRatedMovies(int startPage, int endPage) {
//        Log.d(TAG, "discoverHighestRatedMovies - start:" + startPage + " end: " + endPage);
        // Start the service to get popular movies in descending order
        discoverMovie(startPage, endPage, SortBy.VOTE_AVERAGE_DESC);
    }

    /***
     * This will get the discover movie from the TMDB service given the parameters passed in
     * @param startPage - this is the starting TMDB DBI discovery page
     * @param endPage - this is the starting TMDB DBI discovery page
     * @param sortBy - which discovery basis should the data return in.
     */
    private void discoverMovie(int startPage, int endPage, SortBy sortBy) {
//        Log.d(TAG, "discoverMovie");
        Intent intent = new Intent(getActivity().getApplicationContext(), IntentServiceDiscoverMovie.class);
        intent.putExtra(IntentServiceDiscoverMovie.INTENT_INPUT_PAGE_START, startPage);
        intent.putExtra(IntentServiceDiscoverMovie.INTENT_INPUT_PAGE_END, endPage);
        intent.putExtra(IntentServiceDiscoverMovie.INTENT_INPUT_SORT_BY, sortBy);
        getActivity().getApplicationContext().startService(intent);
    }

    /***
     * This will get the selected favorites
     */
    private void obtainFavorites() {
//        Log.d(TAG, "obtainFavorites");
        // Start the service
        Intent intent = new Intent(getActivity().getApplicationContext(), IntentServiceObtainFavoriteMovies.class);
        intent.putExtra(IntentServiceObtainFavoriteMovies.INTENT_INPUT_POPULAR_MOVIES_ARRAYLIST, tmdbPopularMovies);
        intent.putExtra(IntentServiceObtainFavoriteMovies.INTENT_INPUT_HIGH_RATED_MOVIES_ARRAYLIST, tmdbHighestRated);
        getActivity().getApplicationContext().startService(intent);
    }

    /***
     * This is called by the listener
     * @param movieType
     */
    public void determineMenuUpdateAction(showMovieType movieType) {
//        Log.d(TAG, "determineMenuUpdateAction - movieType: " + movieType);

        // Don't do anything if they are the same
        if (movieType != sortType) {
//            Log.d(TAG, "determineMenuUpdateAction - swapping to " + movieType);
            // Swap viewable data
            sortType = movieType;
            setMovieList();
        }
    }

    /***
     * This will turn of the refresh icon
     */
    private void refreshLoadComplete() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /***
     * Reset the adapter values, if the passed in movie type = the menu item movie type
     * @param discoverMovies
     * @param showMovieType
     */
    private void resetDataAdapter(ArrayList<DiscoverMovieParcelable> discoverMovies,
                                  showMovieType showMovieType) {
//        Log.d(TAG, "resetDataAdapter");
//        Log.d(TAG, "resetDataAdapter sortType - " + sortType + " movieType: " + showMovieType);
        String movieTypeDesc = "Unknown";

        // Don't do anything, in case we received data back, but it's not what the menu drop down
        // has indicated.
        if (showMovieType == sortType) {
            if (discoverMovies != null) {
//                Log.d(TAG, "resetDataAdapter discoverMovies size: " + discoverMovies.size());
                movieTypeDesc = convertShowMovieTypeToString(showMovieType);

//                Log.d(TAG, "resetDataAdapter Current dataAdapter size: " + dataAdapter.getCount());
                // check the size - if zero, hide the detail fragment; otherwise show it
                if (mainActivity.isInTwoPaneMode()) {
                    if (discoverMovies.size() > 0) {
                        mainActivity.showDetailFragment();
                    } else {
                        mainActivity.hideDetailFragment();
                    }
                }

                // reset the adapter with the new data
                dataAdapter.resetData(discoverMovies, movieTypeDesc);

                // Move reset grid array back up to top, if there is any data.
                if (mGridView.getCount() > 0) {
                    mGridView.post(new Runnable() {
                        @Override
                        public void run() {
                            mGridView.setSelection(0);
                        }
                    });
                }

                // This shows the 1st movie for those that have data
                if (initSortType != null && initSortType == sortType) {
//                    Log.d(TAG, "resetDataAdapter initSortType != null && initSortType == sortType ");
                    if (mainActivity.isInTwoPaneMode()) {
//                        Log.d(TAG, "( mainActivity.isInTwoPaneMode() || mainActivity.isDetailFragmentAttached()");
                        initSortType = null;

                        // only go on, if we have data
                        if (discoverMovies.size() > 0) {
                            // get the movie for the first position
                            final DiscoverMovieParcelable movieParcelable = discoverMovies.get(0);
                            mGridView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
//                                    Log.d(TAG, "resetDataAdapter postDelayed runnable - isInTwoPaneMode()");
                                    if (mGridView.getCount() > 0) {
                                        String networkMoviePicLocation = dataAdapter.getMovieImageUriString(0);
//                                        Log.d(TAG, "resetDataAdapter - isInTwoPaneMode - URI: " + networkMoviePicLocation);
                                        Ion.with(getActivity()).load(networkMoviePicLocation).withBitmap().asBitmap()
                                                .setCallback(new FutureCallback<Bitmap>() {
                                                    @Override
                                                    public void onCompleted(Exception e, Bitmap result) {
//                                                        Log.d(TAG, "resetDataAdapter - Ion.onCompleted:: width: " +
//                                                          result.getWidth() + " height: " + result.getHeight() );
                                                        mainActivity.onItemSelected(result, movieParcelable, 0, 0);
                                                    }
                                                });
                                    }
                                }
                            }, 100);
                        }
                    } else {
//                        Log.d(TAG, "resetDataAdapter !mainActivity.isInTwoPaneMode()");
                        // If the fragment exists, but we are in portrait mode, replace the hidden
                        // detail fragment with the 1st (or 0) position detail. It should display
                        // when it's rotated again.
                        if (mainActivity.isDetailFragmentAttached()) {
                            // only go on, if we have data
                            if (discoverMovies.size() > 0) {
                                // get the movie for the first position
                                final DiscoverMovieParcelable movieParcelable = discoverMovies.get(0);
                                mGridView.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
//                                        Log.d(TAG, "resetDataAdapter postDelayed runnable - isDetailFragmentAttached()");
                                        if (mGridView.getCount() > 0) {
                                            // Can't do this, as the bitmap at this point is pointing to the wrong thing!
                                            // Need to change this to a background fetch.
//                                            Bitmap myBitmap = null;

                                            String networkMoviePicLocation = dataAdapter.getMovieImageUriString(0);
//                                            Log.d(TAG, "resetDataAdapter - isDetailFragmentAttached - URI: " + networkMoviePicLocation);

                                            if (networkMoviePicLocation == null) {
                                                Bitmap myBitmap = BitmapFactory.decodeResource(getResources(),
                                                        R.drawable.movie_poster_w154_missing);
                                                mainActivity.setDetailFragData(movieParcelable, myBitmap);
                                            } else {
                                                Ion.with(getActivity()).load(networkMoviePicLocation).withBitmap().asBitmap()
                                                        .setCallback(new FutureCallback<Bitmap>() {
                                                            @Override
                                                            public void onCompleted(Exception e, Bitmap result) {
                                                                mainActivity.saveFileForDetailedIntent(result, MainActivity.DETAIL_FILENAME_FOR_FRAGMENT);
                                                                mainActivity.setDetailFragData(movieParcelable, result);
                                                            }
                                                        });
                                            }
                                        }
                                    }
                                }, 100);
                            }
//                        } else {
//                            Log.d(TAG, "resetDataAdapter !mainActivity.isInTwoPaneMode() && !isDetailFragmentAttached()");
                        }
                    }
//                } else {
//                    if (initSortType == null) {
//                        Log.d(TAG, "resetDataAdapter initSortType == null");
//                    }
//                    if (initSortType != showMovieType) {
//                        Log.d(TAG, "resetDataAdapter initSortType != showMovieType");
//                    }
                }

                // turn off the refresh
                refreshLoadComplete();
            }
        }
    }

    /***
     * Turn on pull to refresh icon
     */
    private void startPullToRefresh() {
//        Log.d(TAG, "startPullToRefresh");
        if (!mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        }
    }

    /***
     * This will be called when the user wants to requery TMDB movie discovery service.
     */
    private void requeryTMDBDiscoveryMovieData() {
//        Log.d(TAG, "requeryTMDBDiscoveryMovieData");
        startPullToRefresh();

        sortType = getMovieType();
        if (sortType == showMovieType.NOT_SET) {
            // Check to see if the saved location is unset.
            sortType = getMovieTypeByPos(mDropDownMenuPos);
        }
        initSortType = sortType;

        if (showMovieType.POPULAR == sortType) {
            discoverPopularMovies();
        } else if (showMovieType.RATING == sortType) {
            discoverHighestRatedMovies();
        } else if (showMovieType.FAVORITES == sortType) {
            obtainFavorites();
        }
    }

    /******************************************************************************************/
    /* End of helper methods section */
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/

    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /* Start of broadcast receiver method section */
    /******************************************************************************************/

    /***
     * This will register the broadcast receivers to get the message when the service completes
     * one way or another.
     */
    private void registerLocalBroadcastLocation() {
//        Log.d(TAG, "registerLocalBroadcastLocation");
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext())
                .registerReceiver(mFavoriteMessageReceiver,
                        new IntentFilter(IntentServiceObtainFavoriteMovies.INTENT_NAME));
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext())
                .registerReceiver(mFavoriteChangedReceiver,
                        new IntentFilter(DetailActivityFragment.INTENT_NAME));
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext())
                .registerReceiver(mDiscoverMovieReceiver,
                        new IntentFilter(IntentServiceDiscoverMovie.INTENT_NAME));
    }

    /***
     * This will unregister the broadcast receivers when the program is put on pause, so
     * we don't lose it (or for a device change)
     */
    private void unregisterLocalBroadcastLocation() {
//        Log.d(TAG, "unregisterLocalBroadcastLocation");
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext())
                .unregisterReceiver(mFavoriteMessageReceiver);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext())
                .unregisterReceiver(mFavoriteChangedReceiver);
        LocalBroadcastManager.getInstance(getActivity().getApplicationContext())
                .unregisterReceiver(mDiscoverMovieReceiver);
    }

    /***
     * This displays a DialogFragment with one button
     * @param id
     * @param fragment
     * @param fragmentActivity
     * @param fragmentName
     * @param dialogTitle
     * @param dialogMessage
     * @param dialogButtonText
     */
    public static void displayOneButtonFragDialog(int id,
                                                  Fragment fragment,
                                                  FragmentActivity fragmentActivity,
                                                  String fragmentName,
                                                  String dialogTitle,
                                                  String dialogMessage,
                                                  String dialogButtonText) {

        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        Fragment prev = fragmentActivity.getSupportFragmentManager().findFragmentByTag(fragmentName);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        ft.commit();

        DialogFragment dialogFrag = DialogFragmentOneChoice.newInstance(
                id,
                dialogTitle,
                dialogMessage,
                dialogButtonText);
        dialogFrag.setTargetFragment(fragment, id);
        dialogFrag.show(fragmentActivity.getSupportFragmentManager().beginTransaction(),
                fragmentName);
    }

    /***
     * This is where we get the message back from the GeoLocationService.
     */
    private final BroadcastReceiver mFavoriteMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            Log.d(TAG, "mFavoriteMessageReceiver running now...");
            ArrayList<DiscoverMovieParcelable> favoriteMovies = null;
            String errorMessage = "No error messages";
            String status = "No data received";

            String action = intent.getAction();
            Bundle bundle = intent.getExtras();

            if (null != bundle) {
                status = bundle.getString(IntentServiceObtainFavoriteMovies.INTENT_EXTRA_STATUS);
                if (bundle.containsKey(IntentServiceObtainFavoriteMovies.INTENT_EXTRA_ERROR_MSG)) {
                    errorMessage = bundle.getString(IntentServiceObtainFavoriteMovies.INTENT_EXTRA_ERROR_MSG, "");
                }
                if (bundle.containsKey(IntentServiceObtainFavoriteMovies.INTENT_EXTRA_FAVORITES)) {
                    favoriteMovies = bundle.getParcelableArrayList(IntentServiceObtainFavoriteMovies.INTENT_EXTRA_FAVORITES);
                } else {
                    favoriteMovies = new ArrayList<DiscoverMovieParcelable>();
                }

//                Log.d(TAG, "mFavoriteMessageReceiver - Received message of " + status +
//                           " favorite size: " + favoriteMovies.size());
                handleFavoriteServiceReturnWithData(status, errorMessage, favoriteMovies);
            } else {
                String message = "mFavoriteMessageReceiver - This app didn't get any information back from service!";
                Log.d(TAG, message);
                handleFavoriteServiceReturnWOData();
            }
        }
    };

    /**
     * This is what's called if there is some good data
     * @param status IntentService response
     * @param errorMessage Error message, if any
     * @param favoriteMovies ArrayList<DiscoverMovieParcelable>
     */
    private void handleFavoriteServiceReturnWithData(String status,
                                                     String errorMessage,
                                                     ArrayList<DiscoverMovieParcelable> favoriteMovies) {
//        Log.d(TAG, "handleFavoriteServiceReturnWithData");

        if (tmdbFavorites == null) {
            tmdbFavorites = new ArrayList<DiscoverMovieParcelable>();
        }
//
//        if (favoriteMovies != null) {
//            Log.d(TAG, "favoriteMovies size: " + favoriteMovies.size() );
//        }

        tmdbFavorites.clear();
        if (status.equals(IntentServiceObtainFavoriteMovies.FAVORITE_RETRIEVE_WORKED)) {
            tmdbFavorites.addAll(favoriteMovies);
        } else if (status == IntentServiceObtainFavoriteMovies.FAVORITE_RETRIEVE_WORKED_NO_RESULT) {
            tmdbFavorites = null;
            tmdbFavorites = new ArrayList<DiscoverMovieParcelable>();
        } else {
            displayOneButtonFragDialog(
                    123,
                    this,
                    getActivity(),
                    getString(R.string.fetch_favorite_error_frag_name),
                    getString(R.string.fetch_favorite_error_title),
                    errorMessage,
                    getString(R.string.fetch_favorite_error_button_text));
            tmdbFavorites = new ArrayList<DiscoverMovieParcelable>();
        }
        resetDataAdapter(tmdbFavorites, showMovieType.FAVORITES);
    }

    /***
     * This is called if the service comes back w/o any data
     */
    private void handleFavoriteServiceReturnWOData() {
//        Log.d(TAG, "handleFavoriteServiceReturnWOData");
        displayOneButtonFragDialog(
                124,
                this,
                getActivity(),
                getString(R.string.fetch_favorite_error_frag_name),
                getString(R.string.fetch_favorite_error_title),
                getString(R.string.fetch_favorite_error_no_return_data),
                getString(R.string.fetch_favorite_error_button_text));
        resetDataAdapter(new ArrayList<DiscoverMovieParcelable>(), showMovieType.FAVORITES);
    }

    /***
     * Favorite changed listener, called by the Detail Activity Fragment
     */
    private final BroadcastReceiver mFavoriteChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            Log.d(TAG, "mFavoriteChangedReceiver running now...");
            String status = "No data received";

            String action = intent.getAction();
            Bundle bundle = intent.getExtras();

            if (null != bundle) {
                status = bundle.getString(DetailActivityFragment.INTENT_EXTRA_STATUS);

//                Log.d(TAG, "mFavoriteChangedReceiver - Received message of " + status);
                handleFavoriteChangedReturnWithData(status);
            } else {
                String message = "mFavoriteChangedReceiver - This app didn't get any information back from service!";
                Log.d(TAG, message);
                handleFavoriteChangedReturnWOData();
            }
        }
    };

    /***
     * We got a broadcast notice that some favorite changed, so we need to repopulate it
     * @param status
     */
    private void handleFavoriteChangedReturnWithData(String status) {
//        Log.d(TAG, "handleFavoriteChangedReturnWithData");
        if (DetailActivityFragment.FAVORITE_ADDED.equals(status) ||
                DetailActivityFragment.FAVORITE_DELETED.equals(status) ||
                DetailActivityFragment.FAVORITE_CHANGED.equals(status)) {
            obtainFavorites();
        }
    }

    /***
     * This will tell the user that an error occurred because we didn't get any favorite data back
     */
    private void handleFavoriteChangedReturnWOData() {
//        Log.d(TAG, "handleFavoriteChangedReturnWOData");
        displayOneButtonFragDialog(
                125,
                this,
                getActivity(),
                getString(R.string.detail_data_favorite_chg_frag_name),
                getString(R.string.detail_data_favorite_chg_title),
                getString(R.string.detail_data_favorite_chg_no_return_data),
                getString(R.string.detail_data_favorite_chg_button_text));
        resetDataAdapter(new ArrayList<DiscoverMovieParcelable>(), showMovieType.FAVORITES);
    }


    /***
     * Favorite changed listener, called by the Discover Movie intent service after it's done
     */
    private final BroadcastReceiver mDiscoverMovieReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            Log.d(TAG, "mDiscoverMovieReceiver running now...");
            String status = "No data received";
            String errorMessage = "No error messages";
            SortBy sortBy;
            ArrayList<DiscoverMovieParcelable> discoverMovies = null;

            String action = intent.getAction();
            Bundle bundle = intent.getExtras();

            if (null != bundle) {
                status = bundle.getString(IntentServiceDiscoverMovie.INTENT_EXTRA_STATUS);
                sortBy = (SortBy) bundle.get(IntentServiceDiscoverMovie.INTENT_EXTRA_SORT_BY);

                if (bundle.containsKey(IntentServiceDiscoverMovie.INTENT_EXTRA_ERROR_MSG)) {
                    errorMessage = bundle.getString(IntentServiceDiscoverMovie.INTENT_EXTRA_ERROR_MSG, "");
                }
                if (bundle.containsKey(IntentServiceDiscoverMovie.INTENT_EXTRA_DISCOVER)) {
                    discoverMovies = bundle.getParcelableArrayList(IntentServiceDiscoverMovie.INTENT_EXTRA_DISCOVER);
                } else {
                    discoverMovies = new ArrayList<DiscoverMovieParcelable>();
                }

//                Log.d(TAG, "mDiscoverMovieReceiver - Received message of " + status + " sortBy " + sortBy + ", movie size: " + discoverMovies.size());
                handleDiscoverMovieReturnWithData(status, sortBy, errorMessage, discoverMovies);
            } else {
                String message = "mDiscoverMovieReceiver - This app didn't get any information back from service!";
                Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG)
                        .show();
                Log.d(TAG, message);
                handleDiscoverMovieReturnWOData();
            }
        }
    };

    /***
     * This will tell the user that an error occurred because we didn't get any movie data back
     */
    private void handleDiscoverMovieReturnWOData() {
//        Log.d(TAG, "handleDiscoverMovieReturnWOData");
        displayOneButtonFragDialog(
                127,
                this,
                getActivity(),
                getString(R.string.discover_fetch_no_data_frag_name),
                getString(R.string.discover_fetch_no_data_err_title),
                getString(R.string.discover_fetch_no_data_err_message),
                getString(R.string.discover_fetch_no_data_button_text));

        resetDataAdapter(new ArrayList<DiscoverMovieParcelable>(), getMovieTypeOrDefault());
    }

    /***
     * This function determines what is done with the data back from the TMDB service
     * @param status
     * @param sortBy
     * @param errorMessage
     * @param discoverMovies
     */
    private void handleDiscoverMovieReturnWithData(String status,
                                                   SortBy sortBy,
                                                   String errorMessage,
                                                   ArrayList<DiscoverMovieParcelable> discoverMovies) {
//        Log.d(TAG, "handleDiscoverMovieReturnWithData");
//
//        if (discoverMovies != null) {
//            Log.d(TAG, "discoverMovies size: " + discoverMovies.size());
//        }

        if (IntentServiceDiscoverMovie.DISCOVER_RETRIEVE_WORKED.equals(status)) {
            if (SortBy.POPULARITY_DESC.equals(sortBy)) {
                setPopularMovies(discoverMovies);
            } else if (SortBy.VOTE_AVERAGE_DESC.equals(sortBy)) {
                setHighestRating(discoverMovies);
            } else {
                Log.e(TAG, "DISCOVER_RETRIEVE_WORKED - Invalid sortBy returned.");
            }
        } else if (IntentServiceDiscoverMovie.DISCOVER_RETRIEVE_WORKED_NO_RESULT.equals(status)) {
            if (SortBy.POPULARITY_DESC.equals(sortBy)) {
                setPopularMovies(new ArrayList<DiscoverMovieParcelable>());
            } else if (SortBy.VOTE_AVERAGE_DESC.equals(sortBy)) {
                setHighestRating(new ArrayList<DiscoverMovieParcelable>());
            } else {
                Log.e(TAG, "DISCOVER_RETRIEVE_WORKED_NO_RESULT - Invalid sortBy returned.");
            }
        } else {
            displayOneButtonFragDialog(
                    126,
                    this,
                    getActivity(),
                    getString(R.string.discover_fetch_frag_name),
                    getString(R.string.discover_fetch_err_title),
                    errorMessage,
                    getString(R.string.discover_fetch_button_text));
            if (SortBy.POPULARITY_DESC == sortBy) {
                setPopularMovies(new ArrayList<DiscoverMovieParcelable>());
            } else if (SortBy.VOTE_AVERAGE_DESC == sortBy) {
                setHighestRating(new ArrayList<DiscoverMovieParcelable>());
            } else {
                Log.e(TAG, "DISCOVER_RETRIEVE_ERROR  - error occurred.");
            }
        }
    }

    /***
     * This will set the Highest rating movie detail ArrayList
     * @param discoverMovies
     */
    private void setHighestRating(ArrayList<DiscoverMovieParcelable> discoverMovies) {
//        Log.d(TAG, "setHighestRating");
        if (tmdbHighestRated == null) {
            tmdbHighestRated = new ArrayList<DiscoverMovieParcelable>();
        }

        tmdbHighestRated.clear();
        tmdbHighestRated.addAll(discoverMovies);

        // Reset the adapter if we are on this movie type
        resetDataAdapter(tmdbHighestRated, showMovieType.RATING);
    }

    /***
     * This will set the Popular movie detail ArrayList
     * @param discoverMovies
     */
    private void setPopularMovies(ArrayList<DiscoverMovieParcelable> discoverMovies) {
//        Log.d(TAG, "setPopularMovies");
        if (tmdbPopularMovies == null) {
            tmdbPopularMovies = new ArrayList<DiscoverMovieParcelable>();
        }

        tmdbPopularMovies.clear();
        tmdbPopularMovies.addAll(discoverMovies);

        // Reset the adapter if we are on this movie type
        resetDataAdapter(tmdbPopularMovies, showMovieType.POPULAR);
    }
}
