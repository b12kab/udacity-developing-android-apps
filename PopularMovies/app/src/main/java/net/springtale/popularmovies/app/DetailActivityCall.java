package net.springtale.popularmovies.app;

import android.graphics.Bitmap;

import net.springtale.popularmovies.app.TMDB.DiscoverMovieParcelable;

/**
 * Created by Keith on 11/5/15.
 */
interface DetailActivityCall {
    /**
     * DetailFragmentCallback for when an item has been selected.
     */
    void onItemSelected(Bitmap movieImage, DiscoverMovieParcelable discoverMovieParcelable, int position, long id);
    boolean isInTwoPaneMode();
    int getListPosition();
//    boolean isInTwoPaneAndFirstTime();
    boolean doesMainActivityExist();
}
