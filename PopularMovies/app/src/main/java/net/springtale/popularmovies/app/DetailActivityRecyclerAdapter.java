package net.springtale.popularmovies.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.uwetrottmann.tmdb.entities.Review;

import net.springtale.popularmovies.app.TMDB.DiscoverMovieParcelable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.FormatterClosedException;
import java.util.IllegalFormatException;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmIOException;
import io.realm.exceptions.RealmMigrationNeededException;

/**
 * Created by Keith on 12/22/15.
 */
public class DetailActivityRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final String TAG = DetailActivityRecyclerAdapter.class.getSimpleName();

    static final int DETAIL_TYPE = 0;
    static final int SET_RATING_TYPE = 1;
    static final int VIDEO_DETAIL_TYPE = 3;
    static final int REVIEW_DETAIL_TYPE = 4;

    private Context mContext = null;
    private DiscoverMovieParcelable movieParcelable = null;
    private ArrayList<MovieVideoInfo> mMovieVideoList = null;
    private ArrayList<Review> mReviewArrayList = null;
    private Bitmap mMoviePoster = null;
    private boolean favoriteFlag;
//    private boolean favoriteFlagSet;
    private MovieVideoClickListener movieVideoClickListener = null;
    private DetailActivityFragment detailActivityFragment = null;

    private String TMDBUrl = "https://www.themoviedb.org/discover/movie?language=en";

    public DetailActivityRecyclerAdapter() {
        movieVideoClickListener = new MovieVideoClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Log.d(TAG, "DetailActivityRecyclerAdapter onItemClick - position = " + position);

//                  REM  http://vishnurajeevan.com/2015/07/12/using-recyclerview-with-multiple-items.html
// ** 581 upvotes   REM  https://stackoverflow.com/questions/24885223/why-doesnt-recyclerview-have-onitemclicklistener-and-how-recyclerview-is-dif#
//                  REM  https://stackoverflow.com/questions/28115553/recyclerview-not-call-any-adapter-method-oncreateviewholder-onbindviewholder

                    MovieVideoInfo movieVideoInfo = mMovieVideoList.get(position);
                    Log.d(TAG, "Starting intent item for " + movieVideoInfo.getYoutubeTitle());
                    Intent i = new Intent(Intent.ACTION_VIEW).setData(movieVideoInfo.getVideoUri());
                    mContext.startActivity(i);
            }
        };
    }

    public DetailActivityRecyclerAdapter(DetailActivityFragment daf) {
        this();
        this.detailActivityFragment = daf;
    }

    public void clearDetailActivityFragment() {
        this.detailActivityFragment = null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        Log.d(TAG, "onCreateViewHolder() called");

        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        // set the context, if not already set
        if (mContext != null) {
            mContext = parent.getContext();
        }

        switch (viewType) {
            case DETAIL_TYPE:
                View headerView = inflater.inflate(R.layout.item_movie_detail, parent, false);
                holder = new ViewHolderMovieDetail(headerView);
                break;
            case VIDEO_DETAIL_TYPE:
                View videoView = inflater.inflate(R.layout.item_movie_video, parent, false);
                holder = new ViewHolderMovieVideo(videoView, movieVideoClickListener);
                break;
            case REVIEW_DETAIL_TYPE:
                View reviewView = inflater.inflate(R.layout.item_movie_review, parent, false);
                reviewView.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                holder = new ViewHolderMovieReview(reviewView);
                break;
        }

        return holder;
    }

    /***
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Instead of using header.getItemViewType, using the java version. I've found the
        // getItemViewType didn't always work correctly
        if (holder instanceof ViewHolderMovieDetail) {
            displayDetail(holder);
        } else if (holder instanceof ViewHolderMovieVideo) {
            if (mMovieVideoList != null) {
                int location = position - 1;
                ViewHolderMovieVideo viewHolder = (ViewHolderMovieVideo) holder;
                String youtubeTitle = mMovieVideoList.get(location).getYoutubeTitle();
                viewHolder.youtubeTitle.setText(youtubeTitle);
            }
        } else if (holder instanceof ViewHolderMovieReview) {
            if (mReviewArrayList != null) {
                int location = position - 1;
                if (mMovieVideoList != null) {
                    location -= mMovieVideoList.size();
                }
                ViewHolderMovieReview viewHolderMovieReview = (ViewHolderMovieReview) holder;
                viewHolderMovieReview.reviewAuthorName.setText( mReviewArrayList.get(location).author);
                viewHolderMovieReview.reviewText.setText( mReviewArrayList.get(location).content );

                viewHolderMovieReview.reviewText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
//                        Log.i(TAG, "reviewText - onClick");

                        TextView tv = (TextView) arg0;
                        if (tv != null) {
                            tv.setMaxLines(Integer.MAX_VALUE);
                            tv.setEllipsize(null);
                        }
                    }
                });
            }
        }
    }

    /***
     * This will return the count in the array list
     * @return # of elements in the array list
     */
    @Override
    public int getItemCount() {
        int count = 0;

        if (movieParcelable != null) {
            count ++;
        }

        if (mMovieVideoList != null) {
         //   count += 1;
            count += mMovieVideoList.size();
        }

        if (mReviewArrayList != null) {
            count += mReviewArrayList.size();
        }

        return count;
    }

    /***
     * This will return the type to onBindViewHolder
     * Note: If this isn't here, it won't work as expected :(
     * @param position Location on where the position is at on the recycler view
     * @return recycler view type to expand
     */
    @Override
    public int getItemViewType(int position) {

        int itemType = DETAIL_TYPE;

        if (position > 0) {
            if (mMovieVideoList != null) {
                if (mReviewArrayList != null) {
                    if (position < mMovieVideoList.size() + 1) {
                        itemType = VIDEO_DETAIL_TYPE;
                    } else {
                        itemType = REVIEW_DETAIL_TYPE;
                    }
                } else {
                    itemType = VIDEO_DETAIL_TYPE;
                }
            } else if (mReviewArrayList != null ) {
                itemType = REVIEW_DETAIL_TYPE;
            }
        }

        return itemType;
    }

    /******************************************************************************************/
    /* End of adapter base methods */
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/


    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /* Start of display helper methods */
    /******************************************************************************************/

    /***
     * Clear out all data
     */
    public void clearAll() {
        mMoviePoster = null;
        movieParcelable = null;

        clearVideos();
        clearReviews();
    }

    public void clearVideos() {
        if (mMovieVideoList != null && !mMovieVideoList.isEmpty()) {
            mMovieVideoList.clear();
        }
        this.notifyDataSetChanged();
    }

    /***
     * Clear out video and review data
     */
    public void clearReviews() {
        if (mReviewArrayList != null && !mReviewArrayList.isEmpty()) {
            mReviewArrayList.clear();
        }
        this.notifyDataSetChanged();
    }

    /***
     * Set the movies list
     * @param movie - DiscoverMovieParcelable
     * @param context Context, just in case
     */
    public void setMovie(DiscoverMovieParcelable movie, Context context) {
        if (mContext == null) {
            mContext = context;
        }
        movieParcelable = movie;
        this.notifyDataSetChanged();
    }

    /***
     * Set the movie poster to display
     * @param bitmap Image
     */
    public void setMoviePoster(Bitmap bitmap) {
        if (bitmap == null) {
            Log.e(TAG, "setMoviePoster() bitmap to be set was null");
        } else {
            mMoviePoster = bitmap;
        }
    }

    public Bitmap getmMoviePoster() {
        return mMoviePoster;
    }

    /***
     * Set the MovieVideoInfo list
     * @param movieVideoInfoArrayList MovieVideoInfo ArrayList from DiscoverMovieParcelable
     * @param context Context, just in case
     */
    public void setVideo(ArrayList<MovieVideoInfo> movieVideoInfoArrayList, Context context) {
        // if we don't have context yet, set context
        if (mContext == null) {
            mContext = context;
        }

        // if it's null, we need to allocate it.
        if (mMovieVideoList == null) {
            mMovieVideoList = new ArrayList<MovieVideoInfo>();
        }

        // Clear the data
        mMovieVideoList.clear();

        if (movieVideoInfoArrayList != null) {
            // Add the data
            mMovieVideoList.addAll(movieVideoInfoArrayList);
        }
        // Tell the data adapter we've updated the data
        this.notifyDataSetChanged();
    }

    /***
     *
     * @param movieReviewList Review List from DiscoverMovieParcelable
     * @param movieReviewOverallUrl this is the URL straight to the reviews section
     */
    public void setReviewData(List<Review> movieReviewList, String movieReviewOverallUrl) {
        if (movieReviewOverallUrl != null) {
            TMDBUrl = movieReviewOverallUrl;
        }

        // If it's null, allocate it
        if (mReviewArrayList == null) {
            mReviewArrayList = new ArrayList<Review>();
        }

        // Clear the data
        mReviewArrayList.clear();

        if (movieReviewList != null) {
            // Add the data
            mReviewArrayList.addAll(movieReviewList);
        }
        // Tell the data adapter we've updated the data
        this.notifyDataSetChanged();
    }

    /***
     * This is called to populate the CardView
     * @param holder RecyclerView.ViewHolder
     */
    private void displayDetail(RecyclerView.ViewHolder holder) {
//        Log.d(TAG, "displayDetail() called");

        String year = "Unknown";
        String runtime = "Unknown";
        String voteAverageString = "Unknown";

        if (movieParcelable != null) {
            ViewHolderMovieDetail viewHolderMovieDetail = (ViewHolderMovieDetail) holder;

            if (movieParcelable.getRelease_date() != null) {
                // Have to get the Date from a calendar
                try {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(movieParcelable.getRelease_date());
                    year = String.valueOf(cal.get(Calendar.YEAR));
                } catch (ArrayIndexOutOfBoundsException aioob) {
                    aioob.printStackTrace();
                    Log.e(TAG, aioob.getMessage());
                }
            } else {
                year = mContext.getString(R.string.detail_unknown);
            }

            // Format the text
            try {
                if (movieParcelable.getVote_average() == null ||
                        movieParcelable.getVote_average() == Double.NaN) {
                    voteAverageString = mContext.getString(R.string.detail_unknown);
                } else {
                    voteAverageString = String.format("%.1f/10", movieParcelable.getVote_average());
                }
                if (movieParcelable.getRuntime() == null || movieParcelable.getRuntime() == 0) {
                    runtime = mContext.getString(R.string.detail_unknown);
                } else {
                    runtime = String.format("%d min", movieParcelable.getRuntime());
                }
            } catch (IllegalFormatException ife) {
                ife.printStackTrace();
                Log.e(TAG, ife.getMessage());
            } catch (FormatterClosedException fce) {
                fce.printStackTrace();
                Log.e(TAG, fce.getMessage());
            }

            // set the detail movie information
            if (movieParcelable.getTitle() == null ||
                    movieParcelable.getTitle().isEmpty() ||
                    movieParcelable.getTitle().equals("")) {
                viewHolderMovieDetail.getMovieName().setText(mContext.getString(R.string.detail_unknown));
            } else {
                viewHolderMovieDetail.getMovieName().setText(movieParcelable.getTitle());
            }
            if (mMoviePoster != null) {
                viewHolderMovieDetail.getMoviePoster().setImageBitmap(mMoviePoster);
            }
            viewHolderMovieDetail.getMovieYear().setText(year);
            viewHolderMovieDetail.getMovieRunTimeMinutes().setText(runtime);
            viewHolderMovieDetail.getMovieRating().setText(voteAverageString);
            viewHolderMovieDetail.getMovieVotes().setText(movieParcelable.getVote_count() + " " + mContext.getString(R.string.detail_vote_label));
            if (movieParcelable.getOverview() == null ||
                    movieParcelable.getOverview().isEmpty() ||
                    movieParcelable.getOverview().equals("")) {
                viewHolderMovieDetail.getMovieDescription().setText(mContext.getString(R.string.detail_unknown_overview));
            } else {
                viewHolderMovieDetail.getMovieDescription().setText(movieParcelable.getOverview());
            }

            updateFavorite(favoriteFlag, viewHolderMovieDetail.getMovieFavorite());
            viewHolderMovieDetail.getMovieFavorite().setTag(viewHolderMovieDetail.getMovieFavorite());
            getFavorite(movieParcelable.getId(), viewHolderMovieDetail.getMovieFavorite());

            viewHolderMovieDetail.getMovieFavorite().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    ImageView iv = (ImageView) arg0.getTag();

                    if (!favoriteFlag) {
                        setFavorite(movieParcelable.getId());
                    } else {
                        removeFavorite(movieParcelable.getId());
                    }

                    // flip the flag
                    favoriteFlag = !favoriteFlag;
                    updateFavorite(favoriteFlag, iv);
                }
            });
        }
    }

    /***
     * Set the favorite icon to the appropriate value
     * @param isFavorite Which image to set the value - isFavorite = true, set full favorite;
     *                   false, set favorite border
     * @param imageView ImageView to act upon
     */
    protected void updateFavorite(boolean isFavorite, ImageView imageView) {
//        Log.d(TAG, "updateFavorite - " + String.valueOf(isFavorite));
        if (isFavorite) {
            imageView.setImageResource(R.drawable.ic_favorite_black_48dp);
        } else {
            imageView.setImageResource(R.drawable.ic_favorite_border_black_48dp);
        }
    }

    /******************************************************************************************/
    /* End of display helper methods */
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/

    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /* Start of data access / set / delete favorite method section */
    /******************************************************************************************/

    /***
     * Add MovieFavorite item for this movie id to the Realm database
     *
     * @param id movie id to add
     */
    private void setFavorite(final int id) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                determineFavoriteStatus(DetailActivityFragment.favoriteChangeType.ADDED);
                // Get a Realm instance for this thread
                try {
                    Realm realm = Realm.getInstance(mContext);
                    RealmResults<MovieFavorite> results = realm.where(MovieFavorite.class)
                            .equalTo("movieId", id)
                            .findAll();
                    if (results.size() == 0) {
                        realm.beginTransaction();
                        // Create new object to add to the realm object store
                        MovieFavorite movieFavorite = realm.createObject(MovieFavorite.class);
                        movieFavorite.setMovieId(id);
                        realm.commitTransaction();
                    }
                    // close the realm instance
                    realm.close();
                } catch (IllegalArgumentException iae) {
                    Log.e(TAG, "Error - IllegalArgumentException " + iae.toString());
                } catch (RealmMigrationNeededException rmne) {
                    Log.e(TAG, "Error - RealmMigrationNeededException - RealmObject classes no longer match the underlying Realm and it must be migrated.");
                } catch (RealmIOException rioe) {
                    Log.e(TAG, "Error - RealmIOException - " + rioe.toString());
                } catch (Exception e) {
                    Log.e(TAG, "Error " + e.toString(), e);
                }
            }
        });
        thread.start();
    }

    // This will set the detail activity fragment's enum of the favorite change status
    private void determineFavoriteStatus(DetailActivityFragment.favoriteChangeType favoriteChangeType) {
        // Don't do anything - if we don't have a reference back tothe detail activity fragment
        if (detailActivityFragment != null) {
            // Get the current favorite change status
            DetailActivityFragment.favoriteChangeType currentFavoriteStatus = detailActivityFragment.getFavoriteStatus();
            // If it's unset, change it to whatever was passed in
            if (currentFavoriteStatus == DetailActivityFragment.favoriteChangeType.UNSET) {
                detailActivityFragment.setFavoriteStatus(favoriteChangeType);
            } else //noinspection StatementWithEmptyBody
                if (currentFavoriteStatus == DetailActivityFragment.favoriteChangeType.BOTH) {
                /* Do Nothing - as it's already an add & change */
            } else { /* It's either add or delete, set it to both now */
                detailActivityFragment.setFavoriteStatus(DetailActivityFragment.favoriteChangeType.BOTH);
            }
        }
    }

    /***
     * Remove any items from the Realm that match the MovieFavorite
     *
     * @param id movie id to remove
     */
    private void removeFavorite(final int id) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                determineFavoriteStatus(DetailActivityFragment.favoriteChangeType.DELETE);
                // Get a Realm instance for this thread
                try {
                    Realm realm = Realm.getInstance(mContext);
                    RealmResults<MovieFavorite> results = realm.where(MovieFavorite.class)
                            .equalTo("movieId", id)
                            .findAll();
                    if (results.size() > 0) {
                        realm.beginTransaction();
                        // delete the object fromthe realm collection
                        results.clear();
                        realm.commitTransaction();
                    }
                    // close the realm instance
                    realm.close();
                } catch (IllegalArgumentException iae) {
                    Log.e(TAG, "Error - IllegalArgumentException " + iae.toString());
                } catch (RealmMigrationNeededException rmne) {
                    Log.e(TAG, "Error - RealmMigrationNeededException - RealmObject classes no longer match the underlying Realm and it must be migrated.");
                } catch (RealmIOException rioe) {
                    Log.e(TAG, "Error - RealmIOException - " + rioe.toString());
                } catch (Exception e) {
                    Log.e(TAG, "Error " + e.toString(), e);
                }
            }
        });
        thread.start();
    }

    /***
     * Get the data from the Realm database and return it, so that it can set the flag for the user
     * @param id
     */
    private synchronized void getFavorite(final int id, final ImageView imageView) {
//        Log.d(TAG, "getFavorite() called");
//        if (!favoriteFlagSet) {
//            Log.d(TAG, "getFavorite - favoriteFlagSet is false");
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    // Get a Realm instance for this thread
                    boolean favoriteResult = false;
                    try {
                        Realm realm = Realm.getInstance(mContext);
                        RealmResults<MovieFavorite> results = realm.where(MovieFavorite.class).equalTo("movieId", id).findAll();
                        if (results.size() > 0) {
                            favoriteResult = true;
                        }
                        realm.close();
                        // This is the reason for the synchronized!
                        // if no error (that is caught by the catches below), set the favorite flag to whatever it is
                        favoriteFlag = favoriteResult;
//                        favoriteFlagSet = true;
                    } catch (IllegalArgumentException iae) {
                        Log.e(TAG, "Error - IllegalArgumentException " + iae.toString());
                    } catch (RealmMigrationNeededException rmne) {
                        Log.e(TAG, "Error - RealmMigrationNeededException - RealmObject classes no longer match the underlying Realm and it must be migrated.");
                    } catch (RealmIOException rioe) {
                        Log.e(TAG, "Error - RealmIOException - " + rioe.toString());
                    } catch (Exception e) {
                        Log.e(TAG, "Error " + e.toString(), e);
                    }
                }
            });
            thread.start();
            // Wait for the thread to finish up
            try {
                thread.join();
                updateFavorite(favoriteFlag, imageView);
            } catch (Exception e) {
                Log.e(TAG, "Error " + e.toString(), e);
            }
//        } else {
//            Log.d(TAG, "getFavorite - favoriteFlagSet is true");
//            updateFavorite(favoriteFlag, imageView);
//        }
    }

    /******************************************************************************************/
    /* End of data access / set / delete favorite method section */
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
}
