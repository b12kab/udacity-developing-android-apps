package net.springtale.popularmovies.app.TMDB;

/**
 * Created by Keith on 11/7/15.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.uwetrottmann.tmdb.entities.Genre;
import com.uwetrottmann.tmdb.entities.Movie;
import com.uwetrottmann.tmdb.entities.ReviewResultsPage;

import net.springtale.popularmovies.app.MovieVideoInfo;

import java.util.ArrayList;
import java.util.Date;

public class DiscoverMovieParcelable implements Parcelable {
    private final String TAG = DiscoverMovieParcelable.class.getSimpleName();

    public int id;
    public Boolean adult;
    public String backdrop_path;
    public ArrayList<Integer> genre_ids = null;
    public String original_language;
    public String original_title;
    public String imdb_id;

    public String overview;
    public Date release_date;
    public String status;

    public String poster_path;
    public Double popularity;

    public String title;
    public Double vote_average;
    public int vote_count;
    public int revenue;

    public ReviewResultsPage reviewResultsPage = null;
    public ArrayList<MovieVideoInfo> movieVideos = null;
    public ArrayList<Genre> genres = null;
    public String tagline = null;
    public Integer runtime = null;
    public String imdbId = null;


    /*******************************/
    /* Getters / Setters           */
    /*******************************/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public Date getRelease_date() {
        return release_date;
    }

    public void setRelease_date(Date release_date) {
        this.release_date = release_date;
    }

    public Boolean getAdult() {
        return adult;
    }

    public void setAdult(Boolean adult) {
        this.adult = adult;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public ArrayList<Integer> getGenre_ids() {
        return genre_ids;
    }

    public void setGenre_ids(ArrayList<Integer> genre_ids) {
        this.genre_ids = genre_ids;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getVote_average() {
        return vote_average;
    }

    public void setVote_average(Double vote_average) {
        this.vote_average = vote_average;
    }

    public int getVote_count() {
        return vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public ArrayList<MovieVideoInfo> getMovieVideos() {
        return movieVideos;
    }

    public void setMovieVideos(ArrayList<MovieVideoInfo> movieVideos) {
        this.movieVideos = movieVideos;
    }

    public ReviewResultsPage getReviewResultsPage() {
        return reviewResultsPage;
    }

    public void setReviewResultsPage(ReviewResultsPage reviewResultsPage) {
        this.reviewResultsPage = reviewResultsPage;
    }

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Genre> genres) {
        this.genres = genres;
    }

    public void addGenre(Genre genres) {
        if (this.genres == null) {
            this.genres = new ArrayList<Genre>();
        }
        this.genres.add(genres);
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public Integer getRuntime() {
        return runtime;
    }

    public void setRuntime(Integer runtime) {
        this.runtime = runtime;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getImdb_id() {
        return imdb_id;
    }

    public void setImdb_id(String imdb_id) {
        this.imdb_id = imdb_id;
    }

    public int getRevenue() {
        return revenue;
    }

    public void setRevenue(int revenue) {
        this.revenue = revenue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DiscoverMovieParcelable(Integer id,
                                   Boolean adult,
                                   String backdrop_path,
                                   ArrayList<Integer> genre_ids,
                                   String original_language,
                                   String original_title,
                                   String overview,
                                   Date release_date,
                                   String poster_path,
                                   Double popularity,
                                   String title,
                                   Double vote_average,
                                   Integer vote_count) {
        setId(id);
        setAdult(adult);
        setBackdrop_path(backdrop_path);
        setGenre_ids(genre_ids);
        setOriginal_language(original_language);
        setOriginal_title(original_title);
        setOverview(overview);
        setRelease_date(release_date);
        setPoster_path(poster_path);
        setPopularity(popularity);
        setTitle(title);
        setVote_average(vote_average);
        setVote_count(vote_count);
    }

    public static DiscoverMovieParcelable createNewDiscoverMovie(Integer id,
                                                                 Boolean adult,
                                                                 String backdrop_path,
                                                                 ArrayList<Integer> genre_ids,
                                                                 String original_language,
                                                                 String original_title,
                                                                 String overview,
                                                                 Date release_date,
                                                                 String poster_path,
                                                                 Double popularity,
                                                                 String title,
                                                                 Double vote_average,
                                                                 Integer vote_count) {
        return new DiscoverMovieParcelable(id, adult, backdrop_path, genre_ids,
                                           original_language, original_title,
                                           overview, release_date,
                                           poster_path, popularity,
                                           title, vote_average, vote_count);
    }

    public static DiscoverMovieParcelable createNewDiscoverMovie(Movie movie) {
        ArrayList<Integer> genre_id = new ArrayList<Integer>();
        // Loop thru the genre results
        if (movie.genres != null) {
            for (Genre genre : movie.genres) {
                genre_id.add(genre.id);
            }
        }

        return new DiscoverMovieParcelable(movie.id, movie.adult,
                                           movie.backdrop_path, genre_id,
                                           "", movie.original_title,
                                           movie.overview, movie.release_date,
                                           movie.poster_path, movie.popularity,
                                           movie.title, movie.vote_average, movie.vote_count);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        Log.v(TAG, "writeToParcel..." + flags);
        boolean[] adultArray = new boolean[1];

        dest.writeInt(id);
        adultArray[0] = adult;
        dest.writeBooleanArray(adultArray);
        dest.writeString(backdrop_path);
        dest.writeSerializable(genre_ids);
        dest.writeString(original_language);
        dest.writeString(original_title);
        dest.writeString(overview);
        dest.writeSerializable(release_date);
        dest.writeString(poster_path);
        dest.writeDouble(popularity);
        dest.writeString(title);
        dest.writeDouble(vote_average);
        dest.writeInt(vote_count);
    }

    public static final Parcelable.Creator<DiscoverMovieParcelable> CREATOR
            = new Parcelable.Creator<DiscoverMovieParcelable>() {
        public DiscoverMovieParcelable createFromParcel(Parcel in) {
            // Allocate size to get Boolean
            boolean[] adultArray = new boolean[1];
            int id = in.readInt();
            in.readBooleanArray(adultArray);
            boolean adult = adultArray[0];
            String backdropPath = in.readString();
            @SuppressWarnings("unchecked") ArrayList<Integer> genreIds = (ArrayList<Integer>) in.readSerializable();

            String originalLanguage = in.readString();
            String originalTitle = in.readString();
            String overview = in.readString();
            Date releaseDate = (Date) in.readSerializable();
            String posterPath = in.readString();
            Double popularity = in.readDouble();
            String title = in.readString();
            Double voterAverage = in.readDouble();
            int voteCount = in.readInt();

            return createNewDiscoverMovie(id,
                    adult,
                    backdropPath,
                    genreIds,
                    originalLanguage,
                    originalTitle,
                    overview,
                    releaseDate,
                    posterPath,
                    popularity,
                    title,
                    voterAverage,
                    voteCount
                    );
        }

        public DiscoverMovieParcelable[] newArray(int size) {
            return new DiscoverMovieParcelable[size];
        }
    };

    private DiscoverMovieParcelable(Parcel in) {
        boolean[] adultArray = new boolean[1];

        int id = in.readInt();
        setId(id);
        in.readBooleanArray(adultArray);
        boolean adult = adultArray[0];
        setAdult(adult);

        String backdropPath = in.readString();
        @SuppressWarnings("unchecked") ArrayList<Integer> genreIds = (ArrayList<Integer>) in.readSerializable();

        String originalLanguage = in.readString();
        String originalTitle = in.readString();
        String overview = in.readString();
        Date releaseDate = (Date) in.readSerializable();
        String posterPath = in.readString();
        Double popularity = in.readDouble();
        String title = in.readString();
        Double voterAverage = in.readDouble();
        int voteCount = in.readInt();

        setBackdrop_path(backdropPath);
        setGenre_ids(genreIds);
        setOriginal_language(originalLanguage);
        setOriginal_title(originalTitle);
        setOverview(overview);
        setRelease_date(releaseDate);
        setPoster_path(posterPath);
        setPopularity(popularity);
        setTitle(title);
        setVote_average(voterAverage);
        setVote_count(voteCount);
    }
}

