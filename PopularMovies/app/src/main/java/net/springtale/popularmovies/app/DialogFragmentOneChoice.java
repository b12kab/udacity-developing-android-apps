package net.springtale.popularmovies.app;

import android.app.Activity;
import android.app.Dialog;
//import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by Keith on 1/2/16.
 */
public class DialogFragmentOneChoice extends DialogFragment {
    private static final String OC_TITLE = "oc_title";
    private static final String OC_MESSAGE = "oc_message";
    private static final String OC_BUTTON = "oc_button";

    public static DialogFragmentOneChoice newInstance(int id,
                                                      String title,
                                                      String message,
                                                      String buttonText) {
        DialogFragmentOneChoice dialogFragment = new DialogFragmentOneChoice();
        Bundle args = new Bundle();
        args.putInt("id", id);
        args.putString(OC_TITLE, title);
        args.putString(OC_MESSAGE, message);
        args.putString(OC_BUTTON, buttonText);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, 0);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString(OC_TITLE, "Title");
        String message = getArguments().getString(OC_MESSAGE, "Unknown identifier");
        String negButtonText = getArguments().getString(OC_BUTTON, "OK");

        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(negButtonText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                getTargetFragment().onActivityResult(getTargetRequestCode(),
                                        Activity.RESULT_OK, getActivity().getIntent());
                            }
                        }
                )
                .create();
    }

}
