package net.springtale.popularmovies.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.common.io.Files;
import com.uwetrottmann.tmdb.entities.Movie;
import com.uwetrottmann.tmdb.entities.Review;
import com.uwetrottmann.tmdb.entities.ReviewResultsPage;

import net.springtale.popularmovies.app.TMDB.DiscoverMovieParcelable;
import net.springtale.popularmovies.app.TMDB.AsyncTaskMovieFetchDetail;
import net.springtale.popularmovies.app.TMDB.MovieFetchReturnTmdb;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Keith on 12/17/15.
 */
public class DetailActivityFragment extends Fragment implements MovieFetchReturnTmdb {
    private final String TAG = DetailActivityFragment.class.getSimpleName();
    public static final String INTENT_INBOUND_MOVIE = "MOVIE_PARSABLE";
    public static final String INTENT_INBOUND_MOVIE_IMAGE_FILENAME = "MOVIE_IMAGE_FILENAME";
    public static final String INTENT_INBOUND_POSITION = "TOUCH_POSITION";
    public static final String SAVED_STATE_DISCOVER_MOVIE_PARSABLE = "detail_discover_movie";
    public static final String SAVED_STATE_MOVIE_DETAIL_ASKED = "detail_movie_queried";
    public static final String SAVED_STATE_FAVORITE_CHANGE = "detail_movie_favorite_change_status";
    public static final String SAVED_STATE_LOCAL_MOVIE_FILE_EXISTS = "detail_file_saved_state_exists";
    public static final String SAVED_STATE_LOCAL_MOVIE_POSTER_FILENAME = "detailImageSave.png";

    private boolean mHaveSavedData;
    private boolean mFetchDetailQueried;

    public enum favoriteChangeType { UNSET, ADDED, DELETE, BOTH }
    private favoriteChangeType movieFavoriteChangeType = favoriteChangeType.UNSET;

    private DetailActivityCall mDetailActivityCallback = null;
    private DetailActivityRecyclerAdapter mDetailActivityRecycleAdapter = null;

    private DiscoverMovieParcelable mDiscoverMovieParcelableDetailFetch = null;

    private RecyclerView mDetailRecyclerView;
    private ProgressBar mProgressBar;

    /******************************************************************************************/
    /* This is the start of the direct Activity call backs */
    /******************************************************************************************/
    // Activity create - fragment lifecycle created 1
    @Override
    public void onAttach(Activity activity) {
//        Log.d(TAG, "onAttach");
        super.onAttach(activity);

        mDetailActivityCallback = (DetailActivityCall) activity;
//
//        if (activity.getLocalClassName().equals(MainActivity.class.getSimpleName())) {
//            Log.d(TAG, "onAttach activity = " + MainActivity.class.getSimpleName());
//        }
//        Log.d(TAG, "onAttach calling activity = " + activity.getLocalClassName() +
//                " main: " + MainActivity.class.getSimpleName());
    }

    // Activity create - fragment lifecycle created 2
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }


    // Activity create - fragment lifecycle created 3
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        Log.d(TAG, "onCreateView");
//        if (savedInstanceState == null) {
//            Log.d(TAG, "onCreateView - savedInstanceState null");
//        } else {
//            Log.d(TAG, "onCreateView - savedInstanceState not null");
//        }

//        ArrayList<MovieVideoInfo> movieVideoList = new ArrayList<MovieVideoInfo>();
        Bitmap moviePosterBitmap = null;
        boolean hasSavedMoviePoster = false;

        if (savedInstanceState != null) {
            mHaveSavedData = true;
            mFetchDetailQueried = savedInstanceState.getBoolean(SAVED_STATE_MOVIE_DETAIL_ASKED, false);
            if (savedInstanceState.containsKey(SAVED_STATE_DISCOVER_MOVIE_PARSABLE)) {
                mDiscoverMovieParcelableDetailFetch = savedInstanceState.getParcelable(SAVED_STATE_DISCOVER_MOVIE_PARSABLE);
            }

            if (savedInstanceState.containsKey(SAVED_STATE_FAVORITE_CHANGE)) {
                movieFavoriteChangeType = (favoriteChangeType) savedInstanceState.get(SAVED_STATE_FAVORITE_CHANGE);
            }

            // only do this extra check if the parent is MainActivity
            if (mDetailActivityCallback.doesMainActivityExist()) {
                if (savedInstanceState.containsKey(SAVED_STATE_LOCAL_MOVIE_FILE_EXISTS)) {
                    hasSavedMoviePoster = true;
                    moviePosterBitmap = getMoviePosterBitmap(SAVED_STATE_LOCAL_MOVIE_POSTER_FILENAME);
                }
            }
        }

        boolean hasArgumentData = false;
        DiscoverMovieParcelable movieParcelable = null;
        String movieFilename = null;

        Bundle arguments = getArguments();
        if (arguments != null) {
            hasArgumentData = true;
            movieFilename = arguments.getString(INTENT_INBOUND_MOVIE_IMAGE_FILENAME);
//            moviePosterBitmap = getMoviePosterBitmap(movieFilename);
            movieParcelable = arguments.getParcelable(INTENT_INBOUND_MOVIE);
        }

        View rootView = inflater.inflate(R.layout.fragment_details, container, false);
        mDetailRecyclerView = (RecyclerView) rootView.findViewById(R.id.detail_recycler_view);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);

        mDetailRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // Deal with the movie video's & reviews
        mDetailActivityRecycleAdapter = new DetailActivityRecyclerAdapter(this);

        // set the movie detail adapter
        mDetailRecyclerView.setAdapter(mDetailActivityRecycleAdapter);
        mDetailRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        mDetailRecyclerView.setHasFixedSize(true);

        if (hasArgumentData || (mHaveSavedData && mDiscoverMovieParcelableDetailFetch != null)) {
//            setDetailInformation(mDiscoverMovieParcelableDetailFetch, moviePosterBitmap, false, false);
            // If we have anything on the argument list, use it - otherwise use the saved data
            if (hasArgumentData) {
                mDiscoverMovieParcelableDetailFetch = movieParcelable;
                if (mHaveSavedData && hasSavedMoviePoster) {
                    hasSavedMoviePoster = false;
                    removeLocalPosterBitmap(SAVED_STATE_LOCAL_MOVIE_POSTER_FILENAME);
                }
                setDetailInformation(mDiscoverMovieParcelableDetailFetch, false, null, true, movieFilename, false, true);
            } else if (mHaveSavedData) {
                if (mHaveSavedData && hasSavedMoviePoster) {
                    setDetailInformation(mDiscoverMovieParcelableDetailFetch, true, moviePosterBitmap, false, null, false, true);
//                } else {
//                    Log.d(TAG, "onCreateView - no data passed in - !(mHaveSavedData && hasSavedMoviePoster)");
                }
            }
//        } else {
//            Log.d(TAG, "onCreateView - no data passed in - !hasArgumentData && !(mHaveSavedData && mDiscoverMovieParcelableDetailFetch != null)");
        }

        return rootView;
    }


    // Fragment ending to save off data
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
//        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(savedInstanceState);

        if (mDiscoverMovieParcelableDetailFetch != null) {
            savedInstanceState.putParcelable(SAVED_STATE_DISCOVER_MOVIE_PARSABLE, mDiscoverMovieParcelableDetailFetch);
        }

        if (movieFavoriteChangeType != favoriteChangeType.UNSET) {
            savedInstanceState.putSerializable(SAVED_STATE_FAVORITE_CHANGE, movieFavoriteChangeType);
        }

        savedInstanceState.putBoolean(SAVED_STATE_MOVIE_DETAIL_ASKED, mFetchDetailQueried);

        // only do this extra check if the parent is MainActivity
        if (mDetailActivityCallback.doesMainActivityExist()) {
            // Save off the picture to the local filesystem
            Bitmap bitmapToSave = mDetailActivityRecycleAdapter.getmMoviePoster();
            boolean fileWriteWorked = saveMoviePosterBitmap(bitmapToSave, SAVED_STATE_LOCAL_MOVIE_POSTER_FILENAME);
            if (fileWriteWorked) {
                savedInstanceState.putBoolean(SAVED_STATE_LOCAL_MOVIE_FILE_EXISTS, true);
            }
        }
    }

    @Override
    public void onStop() {
//        Log.d(TAG, "onStop");
        super.onStop();
        // If we have any changes to the movie favorite, trigger the main fragment's
        // broadcast receiver to have it recalculate the user's favorite movie
        if (!mDetailActivityCallback.isInTwoPaneMode()) {
//            Log.d(TAG, "onStop - !mDetailActivityCallback.isInTwoPaneMode()");
            sendFavoriteChangeReply();
        }
    }

    @Override
    public void onDestroyView() {
//        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
//        Log.d(TAG, "onDestroy");
        super.onDestroy();
        // unset this instance's instance for better recycling
        mDetailActivityRecycleAdapter.clearDetailActivityFragment();
    }

    @Override
    public void onDetach() {
//        Log.d(TAG, "onDetach");
        super.onDetach();
    }

    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /* Start of fetch movie detail methods section */
    /******************************************************************************************/
    /***
     * This starts the fetch of the detailed information on the movie
     */
    private void loadTMDBDiscoveryMovieData(DiscoverMovieParcelable movieParcelable) {
//        Log.d(TAG, "loadTMDBDiscoveryMovieData");
        AsyncTaskMovieFetchDetail movieFetchDetailAsyncTask = new AsyncTaskMovieFetchDetail(getActivity());
        movieFetchDetailAsyncTask.delegate = this;
        movieFetchDetailAsyncTask.execute(movieParcelable);
        mFetchDetailQueried = true;

        // turn on the progress bar
        mProgressBar.setVisibility(View.VISIBLE);

        // Note: the result is sent to postTmdbDetailMovieResult()
    }

    // This will set the data for the detail adapter
    /***
     * This method will load detail data for a given movie
     * @param movieParcelable
     * @param haveBitmap
     * @param moviePosterBitmap
     * @param haveFilename
     * @param localFilesystemMoviePosterName
     * @param resetData
     * @param loadMovieDetail
     */
    public void setDetailInformation(DiscoverMovieParcelable movieParcelable,
                                     boolean haveBitmap,
                                     Bitmap moviePosterBitmap,
                                     boolean haveFilename,
                                     String localFilesystemMoviePosterName,
                                     boolean resetData,
                                     boolean loadMovieDetail) {
//        Log.d(TAG, "setDetailInformation resetData: " + resetData);

        if (haveBitmap) {
            if (moviePosterBitmap != null) {
                mDetailActivityRecycleAdapter.setMoviePoster(moviePosterBitmap);
            }
        }

        if (haveFilename) {
            if (localFilesystemMoviePosterName != null & !localFilesystemMoviePosterName.isEmpty()) {
                Bitmap bitmap = getMoviePosterBitmap(localFilesystemMoviePosterName);
                mDetailActivityRecycleAdapter.setMoviePoster(bitmap);
//            } else {
//                if (localFilesystemMoviePosterName == null) {
//                    Log.d(TAG, "setDetailInformation localFilesystemMoviePosterName == null");
//                }
//                if (localFilesystemMoviePosterName.isEmpty()) {
//                    Log.d(TAG, "setDetailInformation localFilesystemMoviePosterName.isEmpty() == true");
//                }
            }
        }

        if (movieParcelable != null) {
//            Log.d(TAG, "setDetailInformation movieParcelable != null");
            mDetailRecyclerView.scrollToPosition(0);
            // Movie detail
            mDetailActivityRecycleAdapter.setMovie(movieParcelable, getActivity());

            // Video
            mDetailActivityRecycleAdapter.clearVideos();
            mDetailActivityRecycleAdapter.setVideo(movieParcelable.getMovieVideos(), getActivity());

            // Review
            mDetailActivityRecycleAdapter.clearReviews();
            ReviewResultsPage reviewResultsPage = movieParcelable.getReviewResultsPage();
            if (reviewResultsPage != null) {
                ArrayList<Review> movieReviewList = getReviewArrayList(reviewResultsPage.results);

                if (movieReviewList.size() > 0) {
                    mDetailActivityRecycleAdapter.setReviewData(movieReviewList, reviewResultsPage.url);
                }
            }
        }

        // reset the global data, if asked to do so
        if (resetData && movieParcelable != null) {
            mDiscoverMovieParcelableDetailFetch = movieParcelable;
        }

        // Load the data if we are asked to do so
        if (loadMovieDetail) {
            loadTMDBDiscoveryMovieData(movieParcelable);
        }
    }

    /***
     * This is the callback from detail
     * @param errorOccurred true
     * @param returnMovie Native return of object
     * @param returnDiscoverMovie Modified information with video return
     */
    @Override
    public void postTmdbDetailMovieResult(boolean errorOccurred, Movie returnMovie,
                                          DiscoverMovieParcelable returnDiscoverMovie) {
//        Log.d(TAG, "postTmdbDetailMovieResult");

        mFetchDetailQueried = false;

        if (errorOccurred) {
//            mFetchDetailErrorOccurred = true;
            // reset the variables as they are now junk
            mDiscoverMovieParcelableDetailFetch = null;
        } else {
            setDetailInformation(returnDiscoverMovie, false, null, false, null, true, false);
        }

        mProgressBar.setVisibility(View.GONE);
    }

    /******************************************************************************************/
    /* End of fetch movie detail methods section */
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/

    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /******************************************************************************************/
    /* Start of fragment helper method section */
    /******************************************************************************************/
    /***
     * Gets the movie poster file from the local storage
     * @param movieFileName - movie poster file
     * @return Bitmap
     */
    public Bitmap getMoviePosterBitmap(String movieFileName) {
        Bitmap bitmap = null;
        try {
            File filePath = getActivity().getFileStreamPath(movieFileName);
            byte[] bytes = Files.toByteArray(filePath);

            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

            // Delete file
//            filePath.delete();
        } catch (IOException ioe) {
            ioe.printStackTrace();
            Log.e(TAG, ioe.getMessage());
            // If things go bad, set the image to null to be able to go on
            bitmap = null;
        }

        return bitmap;
    }

    public void removeLocalPosterBitmap(String movieFileName) {
        try {
            File filePath = getActivity().getFileStreamPath(movieFileName);

            // Delete file
            boolean worked = filePath.delete();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
    }

    private boolean saveMoviePosterBitmap(Bitmap movieImage, String detailFileName) {
//        Log.d(TAG, "saveMoviePosterBitmap");
        // https://stackoverflow.com/questions/3528735/failed-binder-transaction
        // https://stackoverflow.com/questions/8407336/how-to-pass-drawable-between-activities
        // Have to compress image
        Boolean goodToGo = false;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        movieImage.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] compressedImage = byteArrayOutputStream.toByteArray();

        try {
            FileOutputStream fileOutStream = getActivity().openFileOutput(detailFileName, Context.MODE_PRIVATE);
            fileOutStream.write(compressedImage);
            fileOutStream.close();
            goodToGo = true;
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return goodToGo;
    }

//
//    public void favoriteImageViewClick(View v) {
//        Log.e(TAG, "favoriteImageViewClick()");
//
//    }

    /***
     * Converts a List<Review> into a ArrayList<Review>
     * @param reviewList List<Review>
     * @return ArrayList<Review>
     */
    private ArrayList<Review> getReviewArrayList(List<Review> reviewList){
//        Log.d(TAG, "getReviewArrayList");
        ArrayList<Review> reviewArrayList = new ArrayList<Review>();

        if (reviewList != null) {
            Iterator<Review> reviewIterator = reviewList.iterator();
            while (reviewIterator.hasNext()) {
                Review reviewTemp = reviewIterator.next();
                reviewArrayList.add(reviewTemp);
            }
        }

        return reviewArrayList;
    }

    /***
     * This will be called by the Detail Activity Fragment Adapter
     * @param changeType
     */
    public void setFavoriteStatus(favoriteChangeType changeType) {
//        Log.d(TAG, "setFavoriteStatus");
        movieFavoriteChangeType = changeType;
        // if we are in the main activity that has both the main fragment and the detail fragment
        // we will want to let the main activity to recalculate the favorites
        if (mDetailActivityCallback.isInTwoPaneMode()) {
//            Log.d(TAG, "setFavoriteStatus - mDetailActivityCallback.isInTwoPaneMode()");
            sendFavoriteChangeReply();
        }
    }

    /***
     * This is called by the Detail Activity Fragment Adapter
     * @return
     */
    public favoriteChangeType getFavoriteStatus() {
        return movieFavoriteChangeType;
    }

    public static final String INTENT_NAME = "DetailActivity.favoriteChanged";
    public static final String INTENT_EXTRA_STATUS = "status";
    public static final String FAVORITE_ADDED = "ADD";
    public static final String FAVORITE_DELETED = "DELETE";
    public static final String FAVORITE_CHANGED = "BOTH";

    /***
     * This will send the intent back to the Main Activity Fragment
     */
    private void sendFavoriteChangeReply() {
//        Log.d(TAG, "sendFavoriteChangeReply ");

        Intent intent = new Intent(INTENT_NAME);
        String favoriteType = FAVORITE_ADDED;

        if (movieFavoriteChangeType != favoriteChangeType.UNSET) {
            switch (movieFavoriteChangeType) {
                case DELETE:
                    favoriteType = FAVORITE_DELETED;
                    break;
                case BOTH:
                    favoriteType = FAVORITE_CHANGED;
                    break;
            }
            intent.putExtra(INTENT_EXTRA_STATUS, favoriteType);
//            Log.d(TAG, "sendFavoriteChangeReply - favorite type " + favoriteType);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        }
    }
}
