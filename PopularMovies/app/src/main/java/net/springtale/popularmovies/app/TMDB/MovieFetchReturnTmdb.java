package net.springtale.popularmovies.app.TMDB;

import com.uwetrottmann.tmdb.entities.Movie;

/**
 * Created by Keith on 11/19/15.
 */
public interface MovieFetchReturnTmdb {
    void postTmdbDetailMovieResult(boolean errorOccurred,
                                   Movie returnMovie,
                                   DiscoverMovieParcelable returnDiscoverMovie);
}
