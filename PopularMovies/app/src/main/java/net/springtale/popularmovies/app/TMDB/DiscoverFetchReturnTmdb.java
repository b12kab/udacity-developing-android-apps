package net.springtale.popularmovies.app.TMDB;

import java.util.ArrayList;

/**
 * Created by Keith on 10/6/15.
 */
public interface DiscoverFetchReturnTmdb {
    void postTmdbDiscoverResult(boolean errorOccurred,
                                ArrayList<DiscoverMovieParcelable> popularMovies,
                                ArrayList<DiscoverMovieParcelable> highestRated,
                                String baseUrl,
                                String secureBaseUrl,
                                ArrayList<String> backdropSizes,
                                ArrayList<String> logoSizes,
                                ArrayList<String> posterSizes,
                                ArrayList<String> profileSizes);
}
