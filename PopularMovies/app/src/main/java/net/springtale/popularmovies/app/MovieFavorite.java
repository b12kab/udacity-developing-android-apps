package net.springtale.popularmovies.app;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Keith on 12/18/15.
 */
public class MovieFavorite extends RealmObject {
    @PrimaryKey
    private int             movieId;

//    MovieFavorite() { }
//
//    MovieFavorite(int id) {
//        this.setMovieId(id);
//    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }
}
