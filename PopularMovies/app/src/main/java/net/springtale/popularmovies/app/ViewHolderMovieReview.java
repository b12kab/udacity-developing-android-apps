package net.springtale.popularmovies.app;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Keith on 12/30/15.
 */
public class ViewHolderMovieReview extends RecyclerView.ViewHolder //implements View.OnClickListener
{
    String TAG = ViewHolderMovieReview.class.getSimpleName();
    private MovieVideoClickListener mMovieVideoClickListener;
    // each data item is just a string in this case
    public TextView reviewAuthorName;
    public TextView reviewText;
    public View layout;

    public ViewHolderMovieReview(View v) {
        super(v);
        layout = v;
        reviewAuthorName = (TextView) v.findViewById(R.id.review_list_item_author);
        reviewText = (TextView) v.findViewById(R.id.review_list_item_review_text);
    }

//    @Override
//    public void onClick(View v) {
//
//    }
}
