package net.springtale.popularmovies.app;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import net.springtale.popularmovies.app.TMDB.DiscoverMovieParcelable;

import java.util.ArrayList;

/**
 * Created by Keith on 10/4/15.
 * from http://www.learn2crack.com/2014/01/android-custom-gridview.html
 */
public class DataAdapter extends BaseAdapter {
    private static final String TAG = DataAdapter.class.getSimpleName();
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<DiscoverMovieParcelable> tmdbMovies = new ArrayList<DiscoverMovieParcelable>();
    private String mBaseImageURL = null;
//    private String mPictureTypeURI = null;
    private String mPictureTypeSizeURI = null;
    private String mMovieTypeDesc = null;
    private Drawable missingImagePicture = null;

    public DataAdapter(Context c) {
        mContext = c;
        tmdbMovies = new ArrayList<DiscoverMovieParcelable>();
    }

    /***
     * How many items are in the data set represented by this Adapter.
     * @return
     */
    @Override
    public int getCount() {
        int count = 0;
        if (tmdbMovies != null) {
            count = tmdbMovies.size();
        }
        return count;
    }

    /***
     * Get the data item associated with the specified position in the data set.
     * @param position In view
     * @return
     */
    @Override
    public Object getItem(int position) {
        return tmdbMovies.listIterator(position);
    }

    /***
     * Get the row id associated with the specified position in the list.
     * @param position In view
     * @return
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    /***
     * Displays the data at the specified position in the data set.
     * @param position The position of the item within the adapter's data set of the item whose view we want
     * @param convertView The old view to reuse, if possible.
     * @param parent The parent that this view will eventually be attached to.
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        // set the viewholder
        if (view == null) {
            view = mInflater.inflate(R.layout.grid_single, null);
            ViewHolder viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) view.getTag();

        DiscoverMovieParcelable discoverMovieParcelable = tmdbMovies.get(position);
        String movieTitle = discoverMovieParcelable.title;
        String moviePosterPath = discoverMovieParcelable.poster_path;

        // Check to see if the poster path exists
        if (moviePosterPath == null || moviePosterPath.isEmpty() || moviePosterPath.equals(""))
        {
            viewHolder.imageView.setImageDrawable(missingImagePicture);
        }
        else {
            try {
                // deal with the picture image now
                Uri picURI = Uri.parse(mBaseImageURL)
                        .buildUpon()
                        .appendPath(mPictureTypeSizeURI)
                        .build();

                String picFullUri = picURI.toString() + moviePosterPath;
//                Log.d(TAG, "built URI: " + picFullUri + " for movie: " + movieTitle);

                // Use Picasso to load the image
                // Temporarily have a placeholder in case it's slow to load
                Picasso.with(mContext)
                        .load(picFullUri)
                        .placeholder(R.drawable.movie_poster_w154)
                        .error(R.drawable.movie_poster_w154_err)
                        .into(viewHolder.imageView);
            } catch (Exception e) {
                Log.e(TAG, "Error " + e.toString(), e);
                return null;
            }
        }
        return view;
    }

    /***
     * This cache's the imageview item. Will be set in the TAG for the ImageView
     */
    public static class ViewHolder {
        public final ImageView imageView;

        public ViewHolder(View view) {
            imageView = (ImageView) view.findViewById(R.id.grid_image);
        }
    }

    /***
     * This is the method to set the tmdb image base URI.
     * @param pictureBaseURL
     */
    public void setPictureBaseURL(String pictureBaseURL) {
        mBaseImageURL = pictureBaseURL;
    }

    /***
     * This method adds the picture size, which is used to build the tmdb image URI
     * @param sizeURLPortion
     */
    public void setSizeURLPortion(String sizeURLPortion) {
        mPictureTypeSizeURI = sizeURLPortion;
    }

    /***
     * This will clear the movie list, add the passed in movieArray and notify itself that the data changed
     * @param movieArrayList
     */
    public void resetData(ArrayList<DiscoverMovieParcelable> movieArrayList, String source) {
//        Log.d(TAG, "resetData" );
//        if (movieArrayList != null) {
//            Log.d(TAG, "resetData - movieArrayList size: " + movieArrayList.size());
//        }
//        if (mMovieTypeDesc != null) {
//            Log.d(TAG, "resetData - current movie source: " + mMovieTypeDesc + " new source: " + source);
//        } else {
//            Log.d(TAG, "resetData - current movie source: **NULL** new source: " + source);
//        }

        mMovieTypeDesc = source;
        // Clear the data
        tmdbMovies.clear();
        // Add the data
        tmdbMovies.addAll(movieArrayList);
        // Tell the data adapter we've updated the data
        this.notifyDataSetChanged();
    }

    public DiscoverMovieParcelable getMovieDataAtPos(int position) {
        return tmdbMovies.get(position);
    }

    public String getMovieImageUriString(int position) {
        String picFullUri = null;
        //noinspection StatementWithEmptyBody
        if (tmdbMovies.get(position).poster_path == null ||
            tmdbMovies.get(position).poster_path.isEmpty() ||
            tmdbMovies.get(position).poster_path.equals(""))
        {
        }
        else {
            try {
                // deal with the picture image now
                Uri picURI = Uri.parse(mBaseImageURL)
                        .buildUpon()
                        .appendPath(mPictureTypeSizeURI)
                        .build();

                picFullUri = picURI.toString() + tmdbMovies.get(position).poster_path;
//                Log.d(TAG, "built URI: " + picFullUri + " for movie: " + movieTitle);
            } catch (Exception e) {
                Log.e(TAG, "Error " + e.toString(), e);
            }
        }

        return picFullUri;
    }

    public void setInflater(LayoutInflater inflater) {
        mInflater = inflater;
    }

    public void setMissingPoster(Drawable missingPosterDrawable) {
        missingImagePicture = missingPosterDrawable;
    }
}
