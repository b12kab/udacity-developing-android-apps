package net.springtale.popularmovies.app.TMDB;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.uwetrottmann.tmdb.entities.Genre;
import com.uwetrottmann.tmdb.entities.Movie;
import com.uwetrottmann.tmdb.entities.ReviewResultsPage;
import com.uwetrottmann.tmdb.entities.Videos;

import net.springtale.popularmovies.app.MovieVideoInfo;
import net.springtale.popularmovies.app.R;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Keith on 11/5/15.
 */
public class AsyncTaskMovieFetchDetail extends AsyncTask<DiscoverMovieParcelable, Void, Void> {
    private static final String TAG = AsyncTaskMovieFetchDetail.class.getSimpleName();
    private boolean errorOccurred;

    public MovieFetchReturnTmdb delegate = null;
    private Movie movieReturn = null;
    private DiscoverMovieParcelable discoverMovieItem = null;
    private Context context = null;

    public AsyncTaskMovieFetchDetail(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(DiscoverMovieParcelable... movieParcelable) {
        int movieCount = movieParcelable.length;

        if (movieCount > 0) {
            discoverMovieItem = movieParcelable[0];
            try {
//                Log.d(TAG, "Retrofit detail review id: " + discoverMovieItem.id);
                movieReturn = TmdbInfo.tmdb.moviesService().summary(discoverMovieItem.id, "", null);
                Videos videos = TmdbInfo.tmdb.moviesService().videos(discoverMovieItem.id, "");
                movieReturn.videos = videos;
                List<Videos.Video> videoList = videos.results;

                try {
                    ArrayList<MovieVideoInfo> movieVideos = new ArrayList<MovieVideoInfo>();
                    // Loop thru the video results for the movie
                    for (Videos.Video video : videoList) {
                        // We will be building a URI like this
                        // https://www.youtube.com/watch?v=(value)
                        Uri.Builder builder = new Uri.Builder();

                        builder.scheme("https")
                                .authority("www.youtube.com")
                                .appendPath("watch")
                                .appendQueryParameter("v", video.key);

                        Uri videoUri = builder.build();

                        MovieVideoInfo movieVideoInfo = new MovieVideoInfo();
                        // Set the videoUri
                        movieVideoInfo.setVideoUri(videoUri);

//                        Log.d(TAG, movieReturn.title + " video URI built " + videoUri.toString());

                        // Set the Youtube Title
                        movieVideoInfo.setYoutubeTitle(getTitleQuietly(videoUri.toString(), context));

//                        Log.d(TAG, movieReturn.title + " video YouTube title " + movieVideoInfo.getYoutubeTitle());

                        // Add the translated URI to the array
                        movieVideos.add(movieVideoInfo);
                    }
                    discoverMovieItem.setMovieVideos(movieVideos);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }

                ReviewResultsPage reviewResultsPage = TmdbInfo.tmdb.moviesService().reviews(discoverMovieItem.id, 1, "");
                discoverMovieItem.setReviewResultsPage(reviewResultsPage);

                // Check to see if the genre list is empty or null
                if (discoverMovieItem.getGenre_ids() == null ||
                    discoverMovieItem.getGenre_ids().size() == 0) {
                    if (movieReturn.genres != null && movieReturn.genres.size() != 0) {
                        for (Genre genre : movieReturn.genres) {
                            discoverMovieItem.addGenre(genre);
                        }
                    }
                }

                discoverMovieItem.setRevenue(movieReturn.revenue);
                discoverMovieItem.setImdb_id(movieReturn.imdb_id);
                discoverMovieItem.setTagline(movieReturn.tagline);
                discoverMovieItem.setRuntime(movieReturn.runtime);
                discoverMovieItem.setImdbId(movieReturn.imdb_id);

                errorOccurred = false;
            } catch (Exception e) {
                Log.e(TAG, e.toString());
                errorOccurred = true;
            }
//        } else {
//            Log.d(TAG, "No video's found on call");
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
//        Log.d(TAG, "onPostExecute");
        if (delegate != null) {
//            Log.d(TAG, "Sending response");
            delegate.postTmdbDetailMovieResult(errorOccurred, movieReturn, discoverMovieItem);
        } else {
            Log.e(TAG, "Caller failed to register callback reference");
        }
    }

    @Override
    protected void onPreExecute() {}

    @Override
    protected void onProgressUpdate(Void... values) {}

    public String getTitleQuietly(String youtubeUrl, Context myContext) {
        try {
            if (youtubeUrl != null) {
                URL embeddedURL = new URL("http://www.youtube.com/oembed?url=" +
                        youtubeUrl + "&format=json"
                );

                String youtubeTitle = new JSONObject(IOUtils.toString(embeddedURL)).getString("title");

                if (youtubeTitle == null || youtubeTitle.isEmpty()) {
                    youtubeTitle = myContext.getString(R.string.youtube_title_missing);
                }
                return youtubeTitle;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return myContext.getString(R.string.youtube_title_missing);
    }
}
