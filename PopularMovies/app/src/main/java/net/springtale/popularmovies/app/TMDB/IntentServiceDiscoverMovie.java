package net.springtale.popularmovies.app.TMDB;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.uwetrottmann.tmdb.entities.Movie;
import com.uwetrottmann.tmdb.entities.MovieResultsPage;
import com.uwetrottmann.tmdb.enumerations.SortBy;

import java.util.ArrayList;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Keith on 1/3/16.
 */
public class IntentServiceDiscoverMovie extends IntentService {
    private static final String TAG = IntentServiceDiscoverMovie.class.getSimpleName();
    boolean mErrorOccurred;
    int mPageBegin =0;
    int mPageEnd =0;
    String mErrorMessage = "";
    SortBy mSortBy = SortBy.POPULARITY_DESC;
    private ArrayList<DiscoverMovieParcelable> tmdbDiscoverMovies = null;

    public static final String INTENT_INPUT_PAGE_START = "discover_page_start";
    public static final String INTENT_INPUT_PAGE_END = "discover_page_end";
    public static final String INTENT_INPUT_SORT_BY = "discover_sort_by";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public IntentServiceDiscoverMovie() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
//        Log.d(TAG, "onHandleIntent");

        Bundle bundle = intent.getExtras();
        if (null != bundle) {
            mPageBegin = bundle.getInt(INTENT_INPUT_PAGE_START);
            mPageEnd = bundle.getInt(INTENT_INPUT_PAGE_END);
            mSortBy = (SortBy) bundle.get(INTENT_INPUT_SORT_BY);
//            Log.d(TAG, "onHandleIntent - start:" + mPageBegin + " end: "+ mPageEnd + " sort: " + mSortBy.toString());
        } else {
            mErrorMessage = "Missing bundle";
            mErrorOccurred = true;
        }

        if (!mErrorOccurred) {
            obtainData();
        }
        sendReply();
    }

    private void obtainData() {
//        Log.d(TAG, "obtainData");
        int start = mPageBegin;
        int end = mPageEnd;

        // If someone put the order in wrong, swap them around
        if (start > end) {
            int swap = start;
            start = end;
            end = swap;
        }

        tmdbDiscoverMovies = new ArrayList<DiscoverMovieParcelable>();

        // Try loop to keep any retrofit errors constrained
        int page = 0;
        try {
            Integer voteCountGte = null;
            // I've noticed that there are alot of 1 vote 10's, this will take care of those outliers
            if (mSortBy == SortBy.VOTE_AVERAGE_DESC) {
                voteCountGte = 10;
            }

            for(page = start; page <= end; page++) {
                tmdbDiscoverMovies.addAll(getMoviePage(page, mSortBy, voteCountGte));
            }
        } catch (RetrofitError re) {
            Response r = re.getResponse();
            int httpStatus = r.getStatus();
            Log.e(TAG, String.valueOf(httpStatus) + " Http status on page: " + page + " sort:" + mSortBy.toString());
            Log.e(TAG, r.toString());
            mErrorOccurred = true;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            mErrorOccurred = true;
        }
    }

    /***
     * Get the movie data from retrofit
     * @param page TMDB api page
     * @param sortBy TMDB SortBy statement
     * @param voteCountGte always null
     * @return movie list
     */
    private ArrayList<DiscoverMovieParcelable> getMoviePage(int page, SortBy sortBy, Integer voteCountGte) {
        ArrayList<DiscoverMovieParcelable> movieList = new ArrayList<DiscoverMovieParcelable>();

        try {
            MovieResultsPage results = TmdbInfo.tmdb.discoverService().discoverMovie(
                    false,  /* includeAdult */
                    true,   /* includeVideo */
                    null,   /* language */
                    page,     /* page  */
                    null,   /* primaryReleaseYear */
                    null,   /* primaryReleaseYearGte */
                    null,   /* primaryReleaseYearLte */
                    null,   /* releaseDateGte */
                    null,   /* releaseDateLte */
                    sortBy,  /* mSortBy */
                    voteCountGte,   /* voteCountGte */
                    null,   /* voteCountLte */
                    null,   /* voteAverageGte */
                    null,   /* voteAverageLte */
                    null,   /* withCast */
                    null,   /* withCrew */
                    null,   /* withCompanies */
                    null,   /* withGenres */
                    null,   /* withKeywords */
                    null,   /* withPeople */
                    null    /* year */
            );

            // Copy the popular results over to the local results
            for (Movie movie : results.results) {
                DiscoverMovieParcelable movieParcelable = DiscoverMovieParcelable.createNewDiscoverMovie(movie);

//                Log.d(TAG, "Retrofit detail video trailers id: " + movieParcelable.id);

                // Add movie to ArrayList
                movieList.add(movieParcelable);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return movieList;
    }


    private boolean replySent = false;
    public static final String INTENT_NAME = "DiscoverMovie.result";
    public static final String INTENT_EXTRA_STATUS = "status";
    public static final String INTENT_EXTRA_SORT_BY = "sort_by";
    public static final String INTENT_EXTRA_ERROR_MSG = "favorite_movies_error_msg";
    public static final String INTENT_EXTRA_DISCOVER= "discover_movies_parsable_arraylist";
    public static final String DISCOVER_RETRIEVE_WORKED = "GOOD";
    public static final String DISCOVER_RETRIEVE_ERROR = "ERROR";
    public static final String DISCOVER_RETRIEVE_WORKED_NO_RESULT = "NO_DATA";

    /***
     * Create the result to send back to the Service caller.
     */
    private void sendReply() {
//        Log.d(TAG, "sendReply ");
        Intent intent = new Intent(INTENT_NAME);
        String resultType;
        String errorMessage = "";

        if (mErrorOccurred) {
            resultType = DISCOVER_RETRIEVE_ERROR;
            errorMessage = mErrorMessage;
        } else if (tmdbDiscoverMovies == null || tmdbDiscoverMovies.size() == 0) {
            resultType = DISCOVER_RETRIEVE_WORKED_NO_RESULT;
            if (tmdbDiscoverMovies == null) {
                errorMessage = "Movie favorites is null";
            } else if (tmdbDiscoverMovies.size() == 0) {
                errorMessage = "Movie favorites size = 0";
            }
        } else {
            resultType = DISCOVER_RETRIEVE_WORKED;
        }

        intent.putExtra(INTENT_EXTRA_STATUS, resultType);
        intent.putExtra(INTENT_EXTRA_SORT_BY, mSortBy);
        if (!errorMessage.isEmpty()) {
            intent.putExtra(INTENT_EXTRA_ERROR_MSG, errorMessage);
        }
        if (tmdbDiscoverMovies != null || tmdbDiscoverMovies.size() > 0) {
            intent.putParcelableArrayListExtra(INTENT_EXTRA_DISCOVER, tmdbDiscoverMovies);
        }
//        Log.d(TAG, "sendReply - sort: " + mSortBy + " tmdbDiscoverMovies size " + tmdbDiscoverMovies.size());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        // reset flag
        replySent = true;
    }
}
