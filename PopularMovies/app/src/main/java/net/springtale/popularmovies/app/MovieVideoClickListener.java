package net.springtale.popularmovies.app;

import android.view.View;

/**
 * Created by Keith on 12/29/15.
 */
public interface MovieVideoClickListener {
    void onItemClick(int position, View v);
}
