package net.springtale.popularmovies.app;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Keith on 12/17/15.
 */
public class ViewHolderMovieDetail extends RecyclerView.ViewHolder  {
    private TextView movieName;
    private ImageView moviePoster;
    private TextView movieYear;
    private TextView movieRunTimeMinutes;
    private TextView movieRating;
    private TextView movieVotes;
    private TextView movieDescription;
    private ImageButton movieFavorite;
    private CardView movieDetailInfo;
    private View v;

    public ViewHolderMovieDetail(View itemView) {
        super(itemView);
        v = itemView;
        movieDetailInfo = (CardView) itemView.findViewById(R.id.detail_movie_info);
        movieName = (TextView) itemView.findViewById(R.id.movie_name);
        moviePoster = (ImageView) itemView.findViewById(R.id.movie_poster);
        movieYear = (TextView) itemView.findViewById(R.id.movie_year);
        movieRunTimeMinutes = (TextView) itemView.findViewById(R.id.movie_run_time);
        movieRating =  (TextView) itemView.findViewById(R.id.movie_rating);
        movieVotes = (TextView) itemView.findViewById(R.id.movie_votes);
        movieDescription = (TextView) itemView.findViewById(R.id.movie_description);
        movieFavorite = (ImageButton)  itemView.findViewById(R.id.image_btn_favorite);
    }

    public TextView getMovieDescription() {
        return movieDescription;
    }

    public TextView getMovieName() {
        return movieName;
    }

    public ImageView getMoviePoster() {
        return moviePoster;
    }

    public TextView getMovieRating() {
        return movieRating;
    }

    public TextView getMovieRunTimeMinutes() {
        return movieRunTimeMinutes;
    }

    public TextView getMovieYear() {
        return movieYear;
    }

    public TextView getMovieVotes() {
        return movieVotes;
    }

    public ImageButton getMovieFavorite() { return movieFavorite; }

    public CardView getMovieDetailInfo() {
        return movieDetailInfo;
    }

    public View getV() {
        return v;
    }
}
