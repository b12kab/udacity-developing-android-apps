package net.springtale.popularmovies.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import net.springtale.popularmovies.app.TMDB.DiscoverMovieParcelable;

public class DetailActivity extends AppCompatActivity implements DetailActivityCall {
    private final String TAG = DetailActivity.class.getSimpleName();
    public static final String INTENT_NAME = "DetailActivity.result";
    public static final String INTENT_EXTRA_MOVIE = "detail_movie";
    public static final String INTENT_EXTRA_POSITION = "detail_position";
    public static final String INTENT_EXTRA_LOCAL_IMAGE_FILE_NAME = "detail_image";

    private int mGridPosition = 0;
    private String mMoviePosterFilename = null;
    private DiscoverMovieParcelable mDiscoverMovieParcelable = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        mGridPosition = intent.getIntExtra(INTENT_EXTRA_POSITION, 0);
        mMoviePosterFilename = intent.getStringExtra(INTENT_EXTRA_LOCAL_IMAGE_FILE_NAME);
        mDiscoverMovieParcelable = intent.getParcelableExtra(INTENT_EXTRA_MOVIE);

        setContentView(R.layout.activity_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.

            Bundle arguments = new Bundle();
            arguments.putInt(DetailActivityFragment.INTENT_INBOUND_POSITION, mGridPosition);
            arguments.putString(DetailActivityFragment.INTENT_INBOUND_MOVIE_IMAGE_FILENAME, mMoviePosterFilename);
            arguments.putParcelable(DetailActivityFragment.INTENT_INBOUND_MOVIE, mDiscoverMovieParcelable);

            DetailActivityFragment detailActivityFragment = new DetailActivityFragment();
            detailActivityFragment.setArguments(arguments);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.detail_activity, detailActivityFragment)
                    .commit();
        }
    }

    @Override
    public void onItemSelected(Bitmap movieImage, DiscoverMovieParcelable discoverMovieParcelable,
                               int position, long id) {
        // Do not put anything in here!
    }

    @Override
    public boolean isInTwoPaneMode() {
        return false;
    }

    @Override
    public int getListPosition() {
        return mGridPosition;
    }

//    @Override
//    public boolean isInTwoPaneAndFirstTime() {
//        return false;
//    }

    /***
     * This will tell the detail fragment, if this is the main activity or not
     * @return
     */
    @Override
    public boolean doesMainActivityExist() {
        return false;
    }
}
