package io.realm;


import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.RealmFieldType;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Table;
import io.realm.internal.TableOrView;
import io.realm.internal.android.JsonUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.springtale.popularmovies.app.MovieFavorite;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MovieFavoriteRealmProxy extends MovieFavorite
    implements RealmObjectProxy {

    static final class MovieFavoriteColumnInfo extends ColumnInfo {

        public final long movieIdIndex;

        MovieFavoriteColumnInfo(String path, Table table) {
            final Map<String, Long> indicesMap = new HashMap<String, Long>(1);
            this.movieIdIndex = getValidColumnIndex(path, table, "MovieFavorite", "movieId");
            indicesMap.put("movieId", this.movieIdIndex);

            setIndicesMap(indicesMap);
        }
    }

    private final MovieFavoriteColumnInfo columnInfo;
    private static final List<String> FIELD_NAMES;
    static {
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("movieId");
        FIELD_NAMES = Collections.unmodifiableList(fieldNames);
    }

    MovieFavoriteRealmProxy(ColumnInfo columnInfo) {
        this.columnInfo = (MovieFavoriteColumnInfo) columnInfo;
    }

    @Override
    @SuppressWarnings("cast")
    public int getMovieId() {
        realm.checkIfValid();
        return (int) row.getLong(columnInfo.movieIdIndex);
    }

    @Override
    public void setMovieId(int value) {
        realm.checkIfValid();
        row.setLong(columnInfo.movieIdIndex, value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if (!transaction.hasTable("class_MovieFavorite")) {
            Table table = transaction.getTable("class_MovieFavorite");
            table.addColumn(RealmFieldType.INTEGER, "movieId", Table.NOT_NULLABLE);
            table.addSearchIndex(table.getColumnIndex("movieId"));
            table.setPrimaryKey("movieId");
            return table;
        }
        return transaction.getTable("class_MovieFavorite");
    }

    public static MovieFavoriteColumnInfo validateTable(ImplicitTransaction transaction) {
        if (transaction.hasTable("class_MovieFavorite")) {
            Table table = transaction.getTable("class_MovieFavorite");
            if (table.getColumnCount() != 1) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field count does not match - expected 1 but was " + table.getColumnCount());
            }
            Map<String, RealmFieldType> columnTypes = new HashMap<String, RealmFieldType>();
            for (long i = 0; i < 1; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }

            final MovieFavoriteColumnInfo columnInfo = new MovieFavoriteColumnInfo(transaction.getPath(), table);

            if (!columnTypes.containsKey("movieId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Missing field 'movieId' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().");
            }
            if (columnTypes.get("movieId") != RealmFieldType.INTEGER) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Invalid type 'int' for field 'movieId' in existing Realm file.");
            }
            if (table.isColumnNullable(columnInfo.movieIdIndex)) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Field 'movieId' does support null values in the existing Realm file. Use corresponding boxed type for field 'movieId' or migrate using io.realm.internal.Table.convertColumnToNotNullable().");
            }
            if (table.getPrimaryKey() != table.getColumnIndex("movieId")) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Primary key not defined for field 'movieId' in existing Realm file. Add @PrimaryKey.");
            }
            if (!table.hasSearchIndex(table.getColumnIndex("movieId"))) {
                throw new RealmMigrationNeededException(transaction.getPath(), "Index not defined for field 'movieId' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex().");
            }
            return columnInfo;
        } else {
            throw new RealmMigrationNeededException(transaction.getPath(), "The MovieFavorite class is missing from the schema for this Realm.");
        }
    }

    public static String getTableName() {
        return "class_MovieFavorite";
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    @SuppressWarnings("cast")
    public static MovieFavorite createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        MovieFavorite obj = null;
        if (update) {
            Table table = realm.getTable(MovieFavorite.class);
            long pkColumnIndex = table.getPrimaryKey();
            if (!json.isNull("movieId")) {
                long rowIndex = table.findFirstLong(pkColumnIndex, json.getLong("movieId"));
                if (rowIndex != TableOrView.NO_MATCH) {
                    obj = new MovieFavoriteRealmProxy(realm.schema.getColumnInfo(MovieFavorite.class));
                    obj.realm = realm;
                    obj.row = table.getUncheckedRow(rowIndex);
                }
            }
        }
        if (obj == null) {
            if (json.has("movieId")) {
                if (json.isNull("movieId")) {
                    obj = realm.createObject(MovieFavorite.class, null);
                } else {
                    obj = realm.createObject(MovieFavorite.class, json.getInt("movieId"));
                }
            } else {
                obj = realm.createObject(MovieFavorite.class);
            }
        }
        if (json.has("movieId")) {
            if (json.isNull("movieId")) {
                throw new IllegalArgumentException("Trying to set non-nullable field movieId to null.");
            } else {
                obj.setMovieId((int) json.getInt("movieId"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    public static MovieFavorite createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        MovieFavorite obj = realm.createObject(MovieFavorite.class);
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("movieId")) {
                if (reader.peek() == JsonToken.NULL) {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field movieId to null.");
                } else {
                    obj.setMovieId((int) reader.nextInt());
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        return obj;
    }

    public static MovieFavorite copyOrUpdate(Realm realm, MovieFavorite object, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        if (object.realm != null && object.realm.getPath().equals(realm.getPath())) {
            return object;
        }
        MovieFavorite realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(MovieFavorite.class);
            long pkColumnIndex = table.getPrimaryKey();
            long rowIndex = table.findFirstLong(pkColumnIndex, object.getMovieId());
            if (rowIndex != TableOrView.NO_MATCH) {
                realmObject = new MovieFavoriteRealmProxy(realm.schema.getColumnInfo(MovieFavorite.class));
                realmObject.realm = realm;
                realmObject.row = table.getUncheckedRow(rowIndex);
                cache.put(object, (RealmObjectProxy) realmObject);
            } else {
                canUpdate = false;
            }
        }

        if (canUpdate) {
            return update(realm, realmObject, object, cache);
        } else {
            return copy(realm, object, update, cache);
        }
    }

    public static MovieFavorite copy(Realm realm, MovieFavorite newObject, boolean update, Map<RealmObject,RealmObjectProxy> cache) {
        MovieFavorite realmObject = realm.createObject(MovieFavorite.class, newObject.getMovieId());
        cache.put(newObject, (RealmObjectProxy) realmObject);
        realmObject.setMovieId(newObject.getMovieId());
        return realmObject;
    }

    public static MovieFavorite createDetachedCopy(MovieFavorite realmObject, int currentDepth, int maxDepth, Map<RealmObject, CacheData<RealmObject>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<MovieFavorite> cachedObject = (CacheData) cache.get(realmObject);
        MovieFavorite standaloneObject;
        if (cachedObject != null) {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return cachedObject.object;
            } else {
                standaloneObject = cachedObject.object;
                cachedObject.minDepth = currentDepth;
            }
        } else {
            standaloneObject = new MovieFavorite();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmObject>(currentDepth, standaloneObject));
        }
        standaloneObject.setMovieId(realmObject.getMovieId());
        return standaloneObject;
    }

    static MovieFavorite update(Realm realm, MovieFavorite realmObject, MovieFavorite newObject, Map<RealmObject, RealmObjectProxy> cache) {
        return realmObject;
    }

    @Override
    public String toString() {
        if (!isValid()) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("MovieFavorite = [");
        stringBuilder.append("{movieId:");
        stringBuilder.append(getMovieId());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        String realmName = realm.getPath();
        String tableName = row.getTable().getName();
        long rowIndex = row.getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieFavoriteRealmProxy aMovieFavorite = (MovieFavoriteRealmProxy)o;

        String path = realm.getPath();
        String otherPath = aMovieFavorite.realm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;;

        String tableName = row.getTable().getName();
        String otherTableName = aMovieFavorite.row.getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (row.getIndex() != aMovieFavorite.row.getIndex()) return false;

        return true;
    }

}
